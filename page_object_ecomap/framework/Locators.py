from selenium.webdriver.common.by import By
from framework.Dictionary import DICTIONARY as test_data
import os

BASE_URL = os.environ.get('ECOMAP_BASE_URL')


class CommonLocator:
    SUCCESS_POPUP = (By.XPATH, '//*[@id="toast-container"][1]')


class LogoLocator:
    LOGO = (By.XPATH, "//img[contains(@src, 'logo.png')]")


class NavigationLocator:
    NAV_BTN = (By.XPATH, "//button[@data-target='#navMenu']")
    RESOURCE = (By.XPATH, "//a[@ng-show='faqTitles']")
    STATISTIC = (By.XPATH, "//a[contains(@href, 'statistic')]")
    ADD_PROBLEM = (By.XPATH, '//*[@href="/#/addProblem"]')
    MOBILE_NAVIGATION = (By.XPATH, "/html/body/div/nav/div/div[1]/button")


class MapLocator:
    MAP = (By.ID, "map")
    MAP_IMG = (By.XPATH, "//*[@id='map']//div[14]/img")
    MAP_IMG_MOBILE = (By.XPATH, "//*[@id='map']//div[3]/img")


class Filter:
    FILTER_BTN = (By.XPATH, "//div[@class='filter-toogle']")
    # filter form


class HomePageLocator(LogoLocator, NavigationLocator, MapLocator):
    URL = BASE_URL + "/#/map"
    LOG_IN = (By.XPATH, "//a[contains(@href,'login')]")
    REGISTER = (By.XPATH, "//a[contains(@href,'register')]")
    USER_PROFILE = (By.XPATH, "//a[@href='/#/user_profile/info']")
    STATISTIC_PAGE_UNREGISTERED = "ul.nav:nth-child(1) > li:nth-child(2) > a:nth-child(1)"


class LoginPageLocator:
    EMAIL = (By.XPATH, "//input[@type='email']")
    PASSWORD = (By.XPATH, "//input[@type='password']")
    SUBMIT = (By.XPATH, "//button[@type='submit']")
    URL = BASE_URL + "/#/login"
    MOBILE_EMAIL = (By.XPATH, "//input[@type='email']")
    MOBILE_PASSWORD = (By.XPATH, "//input[@type='password']")


class RegisterPageLocator:
    REG_URL = BASE_URL + "/#/register"
    REG_BLOCK = (By.XPATH, '//*[@id="registerForm"]')
    EMAIL = (By.XPATH, '//*[@id="email"]')
    NAME = (By.XPATH, '//*[@id="name"]')
    SURNAME = (By.XPATH, '//*[@id="surname"]')
    NICKNAME = (By.XPATH, '//*[@id="nickname"]')
    PASSWORD = (By.XPATH, '//*[@id="password"]')
    CONFIRMPASSWORD = (By.XPATH, '//*[@id="pass_confirm"]')
    SUBMIT_BUTTON = (By.XPATH, '//*[@id="registerForm"]/div[1]/div[2]/button')
    MESSAGE_EMAIL = (By.XPATH, '//*[@id="registerForm"]/div[1]/div[2]/div[1]/div/p')
    MESSAGE_NAME = (By.XPATH, '//*[@id="registerForm"]/div[1]/div[2]/div[2]/div/p')
    MESSAGE_SURNAME = (By.XPATH, '//*[@id="registerForm"]/div[1]/div[2]/div[3]/div/p')
    MESSAGE_NICKNAME = (By.XPATH, '//*[@id="registerForm"]/div[1]/div[2]/div[4]/div/p')
    MESSAGE_PASSWORD = (By.XPATH, '//*[@id="registerForm"]/div[1]/div[2]/div[5]/div[1]/div/p')
    MESSAGE_CONFIRM_PASSWORD = (By.XPATH, '//*[@id="registerForm"]/div[1]/div[2]/div[5]/div[2]/div/p')


class HomeUserPageLocator(LogoLocator, NavigationLocator, MapLocator):
    URL = BASE_URL + "/#/map"
    USER_PROFILE_LINK = (By.XPATH, '//*[@id="navMenu"]/ul[2]/li[1]/a')
    LOGOUT_LINK = (By.XPATH, "//a[@ng-click='Logout()']")
    USER_CREDENTIALS = (By.XPATH, '//*[@id="navMenu"]/ul[2]/li[1]/a')


class AddProblemPageLocator:
    URL = BASE_URL + '/#/addProblem'
    TITLE = (By.XPATH, '//*[@id="title"]')
    PROBLEMS_LIST = (By.XPATH, '//*[@id="problem_type_id"]/ul/li')
    FOREST_PROBLEM = (By.XPATH, '//ul[@class="items-list"]/li[1]')
    LANDFILLS_PROBLEM = (By.XPATH, '//ul[@class="items-list"]/li[2]')
    ILLEGALLY_CONSTRUCTION_PROBLEM = (By.XPATH, '//ul[@class="items-list"]/li[3]')
    PROBLEM_DESCRIPTION = (By.XPATH, '//*[@id="problemContent"]')
    PROPOSAL = (By.XPATH, '//*[@id="proposal"]')
    NEXT = (By.XPATH, '//*[@name="addProblemForm"]/button')
    PUBLISH = (By.XPATH, '//*[@name="uploadProblemPhoto"]/button')
    SEARCH = (By.XPATH, '//input[@value="Пошук"]')
    ADD_PHOTO_ELEMENT = (By.XPATH, '//*[@class="fa fa-plus"]')
    UPLOAD_PHOTO = (By.XPATH, '//input[@name="file"]')
    CHECK_UPLOADED_PHOTO = (By.XPATH, '//div[@class="thumb"]/img')
    PHOTO_DESCRIPTION = (By.XPATH, '//textarea[@name="description"]')
    CONFIRMATION_MESSAGE = (By.XPATH, '//*[@id="toast-container"]/div[1]/div[2]/div')
    ERROR_MESSAGE = (By.XPATH, '//*[@id="toast-container"]/div/div[2]/div')
    ERROR_EMPTY_LATITUDE = (By.XPATH, '//form[@name="addProblemForm"]/div/div[2]/div/p')
    ERROR_EMPTY_LONGITUDE = (By.XPATH, '//form[@name="addProblemForm"]/div/div[3]/div/p')
    TITLE_ERROR = (By.XPATH, '//form[@name="addProblemForm"]/div[3]/div/p')
    PHOTO_DESCRIPTION_ERROR = (By.XPATH, '//form[@name="addProblemForm"]/div[5]/div/p')
    PROPOSAL_ERROR = (By.XPATH, '//form[@name="addProblemForm"]/div[6]/div/p')


class LocationLocator(object):
    FIND_ME = (By.XPATH, "//*[@class = 'form-group col-lg-6']")
    LATITUDE = (By.XPATH, '//*[@id="latitude"]')
    LONGITUDE = (By.XPATH, '//*[@id="longitude"]')
    LOCATION_WIDGET = (By.XPATH, '//*[@class ="gmnoprint"]')


class UserProfileLocator(object):
    URL = BASE_URL + "/#/user_profile/info"
    OLD_PASS = (By.XPATH, "//input[@id='old_pass']")
    NEW_PASS = (By.XPATH, "//input[@id='new_pass']")
    NEW_PASS_CONFIRM = (By.XPATH, "//input[@id='new_pass_confirm']")
    SUBMIT = (By.XPATH, "//button[@type='submit']")
    SUCCESS_POPUP = (By.XPATH, '//*[@id="toast-container"]/div')
    ERR_MSG_PRESENT = (By.XPATH, '//div[@ng-messages="changePasswordForm.new_pass_confirm.$error"]')
    ERR_MSG_PASS_NOT_MATCH = By.XPATH, (ERR_MSG_PRESENT.__getitem__(1) + '/p[text()="Паролі не співпадають."]')
    ERR_MSG_PASS_IS_NECESSARY = (By.XPATH, ERR_MSG_PRESENT.__getitem__(1) + '/p[text()="Це поле є обов\'язковим."]')
    USER_PHOTO = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div[1]/div[1]/div/span')

class StatisticPageLocator:
    URL = BASE_URL + "/#/statistic"
    COMMENTS_XPATH = "//ul[3][contains(@class,'all-statistic')]/li[1][text() != '']"
    COMMENTS_CSS = "ul.all-statistic:nth-child(3) > li:nth-child(1)"
    COMMENTS_LIST_XPATH = "//ul[@ng-repeat = 'problemcomm in problCommStats']"
    SUBSCRIPTIONS_XPATH = "//ul[2][contains(@class,'all-statistic')]/li[1][text() != '']"
    SUBSCRIPTIONS_CSS = "ul.all-statistic:nth-child(2) > li:nth-child(1)"
    SUBSCRIPTIONS_LIST_XPATH = "//ul[@ng-repeat = 'subscription in subscriptions']"
    PROBLEMS_XPATH = "//ul[1][contains(@class,'all-statistic')]/li[1][text() != '']"
    PROBLEMS_CSS = "ul.all-statistic:nth-child(1) > li:nth-child(1)"
    IN_SEVERITIES_LIST_XPATH = "//ul[@ng-repeat = 'severity in severities']"
    TOP_FIRST_ISSUE = (By.XPATH, "/html/body/div/div[4]/div[1]/div/div[1]/div/div[2]/ul[1]/a")
    EYE = (By.XPATH, '/html/body/div[1]/div[4]/div[2]/div/div[1]/div[1]/div[3]/div/span')

class UserProfileNavigationLocator:
    USER_INFO_TAB = (By.XPATH, "/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[1]/ul/li[1]/a")
    PROBLEMS_TAB = (By.XPATH, '//a[text()="Ecomap проблеми"]')
    SUBSCRIPTIONS_TAB = (By.XPATH, "/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[1]/ul/li[4]/a")
    ADMIN_TAB = (By.XPATH, '//uib-tab-heading[text()[contains(.,"Адміністрування")]]')
    MOBILE_SUBSCRIPTION_TAB = (By.XPATH, "/html/body/div/div[4]/div[1]/div/div/div/div/div/div[1]/ul/li[4]/a")


class UserProfileProblemsLocator(UserProfileNavigationLocator):
    URL = "/#/user_profile/problems"
    HEADER_LABEL = (By.XPATH, "/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/div[1]/h3")
    SPECIFIC_PROBLEM_EDIT_LINK = (By.XPATH, "(//a[@class='one_button'])[%s]")
    FIRST_PROBLEM_EDIT_LINK = (By.XPATH, "/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/div[3]/table/tbody/tr[1]/td[9]/a[1]")
    FIRST_PROBLEM_STATUS = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/div[3]/table/tbody/tr[1]/td[6]')
    TOTAL_AMOUNT_OF_PROBLEMS = (By.XPATH, '//div[@ng-controller="UserProblemTableCtrl"]/p/span')
    EMPTY_PROBLEMS = (By.XPATH, '/html/body/div/div[4]/div[1]/div/div/div/div/div/div[2]/div/h3')

    PROBLEMS_PER_PAGE_DROP_DOWN = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/div[2]/div[2]/select')
    TABLE = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/div[3]/table')
    NEXT_TABLE_PAGE_BUTTON = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/div[4]/div/div/ul/li[7]/a')
    PREVIOUS_TABLE_PAGE_BUTTON = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/div[4]/div/div/ul/li[2]/a')
    FIRST_TABLE_PAGE_BUTTON = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/div[4]/div/div/ul/li[1]/a')
    SECOND_TABLE_PAGE_BUTTON = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/div[4]/div/div/ul/li[4]/a')
    LAST_TABLE_PAGE_BUTTON = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/div[4]/div/div/ul/li[8]/a')
    PROBLEM_TYPE_DROPDOWN = (By.XPATH, '//*[@id="problem_type_id"]/ul/li')
    GARBAGE_DUMP_TYPE = (
    By.XPATH, '/html/body/div/div[4]/div[2]/div[2]/div/div/div[1]/form/div[4]/div[1]/div/div[2]/ul/li[2]/span')
    SUBMIT_BUTTON = (By.XPATH, '/html/body/div/div[4]/div[2]/div[2]/div/div/div[1]/form/button')
    EDIT_SUBMIT_BUTTON = (By.XPATH, '/html/body/div/div[4]/div[2]/div[2]/div/div/div[2]/div/form/button')
    TYPE_CHANGED_SUCCESS_POPUP = (By.XPATH, '//*[@id="toast-container"]/div/div[2]/div')


    class TableRowLocator:
        __TABLE_XPATH__ = '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/div[3]/table'
        SUBMIT_STATUS = __TABLE_XPATH__ + '/tbody/tr[%s]/td[1]/i[1]'
        SUBMIT_STATUS_MESSAGE = __TABLE_XPATH__ + '/tbody/tr[%s]/td[1]/div'
        USER_NAME = __TABLE_XPATH__ + '/tbody/tr[%s]/td[2]'
        USER_NICKNAME = __TABLE_XPATH__ + '/tbody/tr[%s]/td[3]'
        HEADER = __TABLE_XPATH__ + '/tbody/tr[%s]/td[4]'
        PROBLEM_TYPE = __TABLE_XPATH__ + '/tbody/tr[%s]/td[5]'
        PROBLEM_STATUS = __TABLE_XPATH__ + '/tbody/tr[%s]/td[6]'
        CREATION_DATE = __TABLE_XPATH__ + '/tbody/tr[%s]/td[7]'
        SHOW_PROBLEM_LINK = __TABLE_XPATH__ + '/tbody/tr[%s]/td[8]/a'
        EDIT_PROBLEM_LINK = __TABLE_XPATH__ + '/tbody/tr[%s]/td[9]/a[1]'
        DELETE_PROBLEM_LINK = __TABLE_XPATH__ + '/tbody/tr[%s]/td[9]/a[2]/span[1]'


class UserProfileSubscriprionLocator(UserProfileNavigationLocator):
    SUBSCRIPTIONS_INFO = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/h3')
    TITLE_1 = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/table/tbody/tr/td[3]')
    COUNT = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/p/span')
    VIEW = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div/div/div[2]/div/table/tbody/tr/td[8]/a')


class ProblemLocator(LogoLocator, NavigationLocator, MapLocator):
    IMPORTANCE_INFO = (By.XPATH, '/html/body/div[1]/div[4]/div[2]/div/div[1]/div[1]/div[1]/div/div')
    STATUS_INFO = (By.XPATH, '/html/body/div[1]/div[4]/div[2]/div/div[1]/div[1]/div[2]/div/strong/span')
    IMPORTANCE_DROP_DOWN = (By.XPATH, "/html/body/div[1]/div[4]/div[2]/div/div[1]/div[2]/form/div[1]/select")
    STATUS_DROP_DOWN = (By.XPATH, "/html/body/div[1]/div[4]/div[2]/div/div[1]/div[2]/form/div[2]/select")
    CHANGE_BTN = (By.XPATH, "/html/body/div[1]/div[4]/div[2]/div/div[1]/div[2]/form/div[2]/button")
    POP_UP_WINDOW_SUCCESSFUL_CHANGE = (By.XPATH, '//*[@id="toast-container"]/div/div[2]/div')
    POP_UP_WINDOW_TITLE = (By.XPATH, '//*[@id="toast-container"]/div/div[1]')
    POP_UP_WINDOW_MESSAGE = (By.XPATH, '//*[@id="toast-container"]/div/div[2]/div')
    DETAILED_TITLE = (By.XPATH, '/html/body/div[1]/div[4]/div[2]/div/h3')


class NotSubmittedProblemLocator(ProblemLocator):
    SUBMIT_STATUS_DROP_DOWN = (By.XPATH, '/html/body/div[1]/div[4]/div[2]/div/div[1]/div[2]/form/div[3]/select')
    COMMENT_FIELD = (By.XPATH, '/html/body/div[1]/div[4]/div[2]/div/div[1]/div[2]/form/div[4]/textarea')
    CHANGE_BTN = (By.XPATH, '/html/body/div[1]/div[4]/div[2]/div/div[1]/div[2]/form/div[5]/button')


class AdministerTabLocator:
    ISSUE_TYPE_TAB = (By.XPATH, '//a[text()[contains(.,"Тип проблеми")]]')
    FIRST_ISSUE_CHANGE_STATUS_BUTTON = (By.XPATH, "html/body/div[1]/div[4]/div[1]/div/div/div/div[3]/div/div[1]/table/tbody/tr[1]/td[4]/button[2]")
    ISSUE_TYPE_FIELD = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div[3]/div/div[3]/div/div/div[2]/form/div[1]/div/div/input')
    SUBMIT_BUTTON = (By.XPATH, '/html/body/div[1]/div[4]/div[1]/div/div/div/div[3]/div/div[3]/div/div/div[2]/form/div[4]/div/div[2]/button')
    TYPE_CHANGED_SUCCES_POPUP = (By.XPATH, '//*[@id="toast-container"]/div/div[2]/div')


class ProblemsUserProfileLocator:
    PROBLEMS_TAB = (By.XPATH, "//a[text()='Ecomap проблеми']")
    PROBLEMS_LIST = (By.XPATH, "//table[@class='table table-stripped']/tbody/tr")
    LAST_PROBLEM_EDIT_LINK = (By.XPATH, "//table[@class='table table-stripped']/tbody/tr[last()]/td/a[contains(text(), 'редагувати')]")
    EDIT_LINK_BY_PROMLEM_TITLE = (By.XPATH, "//tr[td[text()='" + test_data.get('problem_title') + "']]//a[@ng-click='triggerEditModal(problem.id)']")
    URL = BASE_URL + "/#/user_profile/problems"


class CommentsUserProfileLocator:
    MY_COMMENTS_TAB = (By.XPATH, "//li[@heading='Мої коментарі']")
    URL = BASE_URL + "/#/user_profile/comments"
    LAST_BTN_DISABLED = (By.XPATH, "//li[contains(@class, 'disabled')]/a[text()='Last']")
    LAST_BTN_ENABLED = (By.XPATH, "//li[@class='pagination-last ng-scope']/a[text()='Last']")
    COMMENTS_COUNT = (By.XPATH, "//p[@ng-hide='commentsCount == 0']/span[@class='ng-binding']")

    @staticmethod
    def get_delete_link_locator_by_title(title):
        DELETE_LINK_BY_COMMENT_TITLE = [By.XPATH,
                                        "//span[text()='" + title + "']/parent::div/parent::*//a[contains(@ng-click, 'deleteComment')][1]"]
        return DELETE_LINK_BY_COMMENT_TITLE


class EditProblemLocator:
    COMMENT_BLOCKS = (By.XPATH, "//div[@ng-repeat='comment in comments']")
    COMMENT_TAB = (By.XPATH, "//a[text()='Коментарі']")
    COMMENT_TEXTAREA = (By.XPATH, "//textarea[@placeholder='Ваш коментар']")
    ADD_COMMENT_BTN = (By.XPATH, "//input[@value='Додати коментар']")
    ANONYMOUSLY_CHECKBOX = (By.XPATH, "//input[@ng-model='comment.changeUser']")
    COMMENT_BLOCK = "//div[@ng-repeat='comment in comments'][last()]"
    COMMENT_NICKNAME = (By.XPATH, COMMENT_BLOCK + "//*[contains(@class,'comment-nickname')]")
    COMMENT_DATETIME = (By.XPATH, COMMENT_BLOCK + "//span[@ng-hide='comment.updated_date']")
    COMMENT_TEXT = (By.XPATH, COMMENT_BLOCK + "//div[@class='panel-body comment-block ng-binding']")
    COMMENT_ANSWER_LINK = (By.XPATH, COMMENT_BLOCK + "//div[1]/a[contains(@ng-click,'getSubComments')]")
    COMMENT_EDIT_LINK =(By.XPATH, COMMENT_BLOCK + "//span[@class='fa fa-pencil comment-update']")
    COMMENT_EDIT_LINK_HIDDEN = (By.XPATH, COMMENT_BLOCK + "//span[@class='fa fa-pencil comment-update ng-hide']")
    COMMENT_LINK = (By.XPATH, COMMENT_BLOCK + "//span[@ng-click='makeLink(comment.id)']")

    COMMENT_CANCEL_EDIT_LINK = (By.XPATH, COMMENT_BLOCK + "//a[@class='editComment']")
    NEW_COMMENT_TEXTAREA =  (By.XPATH, COMMENT_BLOCK + "//textarea[@name='newcomment']")
    COMMENT_EDIT_DATE_BLOCK = (By.XPATH, COMMENT_BLOCK + "//span[@ng-show='comment.updated_date'][contains(text(), 'Редаговано · ')]")

    ANSWER_BLOCKS = (By.XPATH, "//div[@ng-repeat='subcomment in subcomments']")
    ANSWER_TEXTAREA = (By.XPATH, "//textarea[@placeholder='Ваша відповідь']")
    ADD_ANSWER_BTN = (By.XPATH, "//input[@value='Додати відповідь']")
    ANSWER_BLOCK = "//div[@ng-repeat='subcomment in subcomments'][last()]"
    ANSWER_NICKNAME = (By.XPATH, ANSWER_BLOCK + "//strong[@class='pull-left comment-nickname ng-binding']")
    ANSWER_TEXT = (By.XPATH, ANSWER_BLOCK + "//div[@ng-bind='subcomment.content']")
    ANSWER_ANONYMOUS_CHECKBOX = (
    By.XPATH, "//div[@ng-repeat='comment in comments'][last()]//input[@ng-model='comment.changeUser']")

class MapItemsLocator:
    ALL_ISSUES = (By.XPATH, '//div[@class="gm-style"]/div[1]/div[4]/div[3]/div[1]')
    PROBLEM_ON_MAP = (By.XPATH, "//*[@id='map']/div/div/div[1]/div[3]/div")
    CHANGE_BUTTON = (By.XPATH, "html/body/div[1]/div[4]/div[2]/div/div[1]/div[2]/form/div[2]/button")
    SUCCESS_POPUP = (By.XPATH, "//*[@id='toast-container']/div")
    CLUSTER_IMAGE = BASE_URL + "//image/clusters/m1.png"

class DetailedProblemLocator:
    URL = BASE_URL + "/#/detailedProblem/17"
    CLOSE_EL = '//*[contains(@class, "fa") and contains(@class ,"close")]'
