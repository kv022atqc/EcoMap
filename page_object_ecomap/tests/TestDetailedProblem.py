import unittest

from framework.pages.DetailedProblemPage import DetailedProblemPage
from framework.pages.HomePage import HomePage
from tests.TestBase import TestBase


class TestDetailedProblem(TestBase):
    @classmethod
    def setUpClass(cls):
        super(TestDetailedProblem, cls).setUpClass()
        cls.home_page = HomePage(cls.driver)
        cls.detailed_problem_page = DetailedProblemPage(cls.driver)

    def test_1_assert_detailed_problem_page_is_open(self):
        self.detailed_problem_page.goToDetailedProblemPage()
        self.assertEqual(self.detailed_problem_page.get_expected_url(), self.detailed_problem_page.get_current_url())

    def test_2_negative_is_close_fixed(self):
        self.assertFalse(self.detailed_problem_page.isCloseDivFixed())

    def test_3_assert_page_closed_by_x(self):
        self.detailed_problem_page.goToDetailedProblemPage()
        self.assertTrue(self.detailed_problem_page.isPageClosed())





