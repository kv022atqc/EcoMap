import unittest

from framework.pages.HomePage import HomePage
from framework.pages.LoginPage import LoginPage
from framework.pages.MapPage import MapPage
from framework.pages.SpikePage import HomeUserPage
from tests.TestBase import TestBase
from framework.Dictionary import DICTIONARY as test_data


class TestEditIssueOnMap(TestBase):
    @classmethod
    def setUpClass(cls):
        super(TestEditIssueOnMap, cls).setUpClass()
        cls.home_page = HomePage(cls.driver)
        cls.user_page = HomeUserPage(cls.driver)
        cls.login_page = LoginPage(cls.driver)
        cls.map_page = MapPage(cls.driver)

    @unittest.expectedFailure
    def test_edit_issue_on_map(self):
        self.home_page.get_login_page()
        self.login_page.login(test_data.get("email"), test_data.get("password"))
        self.map_page.get_on_problem()
        self.map_page.click_on_change_button()
        self.map_page.is_success_popup_present()
