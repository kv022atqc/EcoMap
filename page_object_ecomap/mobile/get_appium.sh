#!/usr/bin/env bash

export ANDROID_HOME="/home/$USER/Android/sdk"
export PATH="${PATH}:${ANDROID_HOME}/tools/bin"

sudo apt update
sudo apt install nodejs npm curl unzip libc6-i386 lib32stdc++6 lib32gcc1 lib32ncurses5 lib32z1
mkdir -p ${ANDROID_HOME}
curl -s https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip > /home/${USER}/tools.zip && \
    unzip /home/${USER}/tools.zip -d ${ANDROID_HOME} && \
    rm -v /home/${USER}/tools.zip

sdkmanager --update

mkdir -p $ANDROID_HOME/licenses/ \
  && echo "8933bad161af4178b1185d1a37fbf41ea5269c55" > $ANDROID_HOME/licenses/android-sdk-license \
  && echo "84831b9409646a918e30573bab4c9c91346d8abd" > $ANDROID_HOME/licenses/android-sdk-preview-license

sdkmanager --package_file=package.txt --verbose

npm install appium
# uncomment and change is you use venv
# source /path/to/selenium/venv/bin/activate
pip install Appium-Python-Client