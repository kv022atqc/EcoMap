from framework.pages.SpikePage import HomeUserPage
from tests.TestBase import TestBase
import unittest
from framework.Dictionary import DICTIONARY as test_data


class MobileTestLoginAsAdmin(TestBase):

    def test_login_as_admin(self):
        self.home_page.get_navigation_button()
        login_page = self.home_page.get_login_page()

        self.assertTrue(login_page.is_email_field_present())
        self.assertTrue(login_page.is_password_field_present())
        self.assertTrue(login_page.is_submit_button_present())

        home_user_page = login_page.login(test_data.get("email"), test_data.get("password"))

        self.assertTrue(home_user_page.is_logout_btn_present())
        self.assertTrue(home_user_page.is_user_profile_link_present())

        home_user_page = HomeUserPage(self.driver)
        home_user_page.get_navigation_button()
        user_profile_page = home_user_page.get_user_profile_page()

        self.assertTrue(user_profile_page.is_admin_tab_present())

if __name__ == '__main__':
    unittest.main()
