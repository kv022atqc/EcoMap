from flask import json
from framework.Utils import *
from locust import HttpLocust, TaskSet, task
from framework.Dictionary import DICTIONARY as test_data


class ChangeRoleLoadTest(TaskSet):

    @task(2)
    def user_profile_page(self):
        with self.client.get("/api/user_detailed_info/1", catch_response=True) as response:
            if response.status_code == 200:
                response.success()

    @task(1)
    def admin_tab(self):
        with self.client.get("/api/roles", catch_response=True) as response:
            if response.status_code ==200:
                response.success()

        with self.client.get("/api/resources?offset=0&per_page=10", catch_response=True) as response:
            if response.status_code ==200:
                response.success()

        with self.client.get("/api/all_permissions", catch_response=True) as response:
            if response.status_code ==200:
                response.success()

        with self.client.get("/api/problem_type", catch_response=True) as response:
            if response.status_code == 200:
                response.success()

        with self.client.get("/api/resources?offset=0&per_page=5", catch_response=True) as response:
            if response.status_code == 200:
                response.success()

    @task(2)
    def open_problem_tab(self):
        with self.client.get("/templates/problemsAdmin.html", catch_response=True) as response:
            if response.status_code == 200:
                response.success()

    @task(1)
    def count_users(self):
        with self.client.get("/api/user_page?offset=0&per_page=5", catch_response=True) as response:
            if response.status_code == 200:
                self.rejson = json.loads(response.text)
                self.count = self.rejson[-1][0]['total_users']

    @task(4)
    def change_role(self):
        self.count_users()
        data = {"role_id": generate_random_number('int', 1, 3), "user_id": generate_random_number('int', 2, self.count)}
        with self.client.post("/api/user_roles", json=data, catch_response=True) as response:
            if response.status_code == 200:
                response.success()


class UserBehaviour(TaskSet):

    tasks = [ChangeRoleLoadTest]
    def on_start(self):
        self.create_users()
        self.sign_in_as_admin()

    def create_users(self):
        data = {"email": test_data.get("registration_email") % generate_random_number('int',1, 10000000),
                "first_name":test_data.get("registration_name"),
                "last_name":test_data.get("registration_surname"),
                "nickname" :test_data.get("registration_nickname") % generate_random_number('int', 1, 1000000),
                "pass_confirm" :test_data.get("registration_password"),
                "password":test_data.get("registration_password")}
        self.client.post("/api/register", json = data)

    def sign_in_as_admin(self):
        data = {"email": test_data.get('email'), "password": test_data.get('password')}
        self.client.get("/#/map")
        self.client.post("/api/login", json=data)


class WebUser(HttpLocust):
    task_set = UserBehaviour
    min_wait = 5 * 1000
    max_wait = 60 * 1000

