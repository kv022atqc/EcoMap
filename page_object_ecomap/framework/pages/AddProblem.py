#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
from math import fabs

import requests
from framework.Locators import LocationLocator, AddProblemPageLocator
from framework.pages.BasePage import BasePage
from framework.Dictionary import DICTIONARY as test_data


class AddProblemPage(BasePage):
    """This is page where user can add new problem"""

    def click_on_find_me(self):
        """click on 'Find me' button and return found coordinates"""
        try:
            self.click(*LocationLocator.FIND_ME)
            latitude = float(self.get_value_from_element(*LocationLocator.LATITUDE))
            longitude = float(self.get_value_from_element(*LocationLocator.LONGITUDE))
            found_coordinates = [latitude, longitude]
            return found_coordinates
        except AssertionError:
            if self.get_actual_coordinates() is not None:
                latitude, longitude = self.get_actual_coordinates()
                self.driver.execute_script('window.navigator.geolocation.getCurrentPosition'
                                           '= function(success){var position = '
                                           '{"coords": {"latitude":' + str(latitude) + ', '
                                            '"longitude":' + str(longitude) + '}};'
                                            'success(position);}')
                return [latitude, longitude]
            else:
                self.driver.execute_script('window.navigator.geolocation.getCurrentPosition'
                                           '= function(success){var position = '
                                           '{"coords": {"latitude":' + test_data.get('latitude') + ', '
                                            '"longitude":' + test_data.get('longitude') + '}};'
                                            'success(position);}')
                return [float(test_data.get('latitude')), float(test_data.get('longitude'))]

    @staticmethod
    def check_location(found_coordinates, actual_coordinates):
        """Validate that coordinates found by applications equal actual coordinates
            or deviates from actual not more than 11 km"""
        try:
            if fabs(actual_coordinates[0] - found_coordinates[0]) < 0.1 \
                    and fabs(actual_coordinates[1] - found_coordinates[1]) < 0.1:
                return True
            else:
                return False
        except (IndexError, TypeError):
            return False

    @staticmethod
    def get_actual_coordinates():
        """return actual coordinates which is found by outside service"""
        try:
            send_url = 'http://freegeoip.net/json/'
            r = requests.get(send_url)
            j = json.loads(r.text)
            lat = j['latitude']
            lon = j['longitude']
            actual_coordinates = [lat, lon]
        except Exception:
            actual_coordinates = None
        return actual_coordinates

    def is_location_widget_present(self):
        """This method return True if location widget is present  on the map"""
        return self.is_element_present(*LocationLocator.LOCATION_WIDGET)

    def is_coordinates_fields_present(self):
        """"This method return True if latitude and longitude fields are present"""
        return self.is_element_present(*LocationLocator.LATITUDE) and \
               self.is_element_present(*LocationLocator.LONGITUDE)

    def is_coordinates_filled(self):
        """"This method return True 
        if latitude and longitude fields are filled with coordinates"""
        latitude = self.driver.find_element(*LocationLocator.LATITUDE).get_attribute("value")
        longitude = self.driver.find_element(*LocationLocator.LONGITUDE).get_attribute("value")
        if len(latitude) > 0 and len(longitude) > 0:
            return True
        else:
            return False

    def is_find_me_button_present(self):
        """This method return True if 'Find me" button is present"""
        return self.is_element_present(*LocationLocator.FIND_ME)

    def fill_coordinates(self, latitude, longitude):
        """This method fills latitude and longitude fields with random data"""
        self.type(latitude, *LocationLocator.LATITUDE)
        self.type(longitude, *LocationLocator.LONGITUDE)

    def get_reason_of_fail(self):
        """Return the reason of why found coordinates don't equal actual coordinates"""
        actual_coordinates = self.get_actual_coordinates()
        found_coordinates = self.click_on_find_me()
        if found_coordinates is None and actual_coordinates is None:
            message = "Can't find coordinates by application and outside service"
        elif actual_coordinates is None:
            message = "Can't find coordinates by outside service"
        elif found_coordinates is None:
            message = "Can't find coordinates by application"
        else:
            message = "Actual coordinates don't equal found coordinates"
        return message

    @staticmethod
    def get_expected_url():
        """This method return URL of AddProblem page"""
        return AddProblemPageLocator.URL

    def is_title_field_present(self):
        """This method return True if title field is present"""
        return self.is_element_present(*AddProblemPageLocator.TITLE)

    def fill_title(self, title):
        """This method fills title field"""
        self.type(title, *AddProblemPageLocator.TITLE)

    def is_title_filled(self):
        """This method return True if title field is filled with data"""
        title = self.driver.find_element(*AddProblemPageLocator.TITLE).get_attribute("value")
        if len(title) > 0:
            return True
        else:
            return False

    def is_problems_items_present(self):
        """This method return True if dropdown list with problems is present"""
        return self.is_element_present(*AddProblemPageLocator.PROBLEMS_LIST)

    def choose_problem(self, type_of_problem='FOREST_PROBLEM'):
        """This method chooses type of problem from problem list"""
        self.click(*AddProblemPageLocator.PROBLEMS_LIST)

        if type_of_problem == 'FOREST_PROBLEM':
            self.click(*AddProblemPageLocator.FOREST_PROBLEM)
        elif type_of_problem == 'LANDFILLS_PROBLEM':
            self.click(*AddProblemPageLocator.LANDFILLS_PROBLEM)
        elif type_of_problem == 'ILLEGALLY_CONSTRUCTION_PROBLEM':
            self.click(*AddProblemPageLocator.ILLEGALLY_CONSTRUCTION_PROBLEM)

    def is_description_filed_present(self):
        """This method return True if description field is present"""
        return self.is_element_present(*AddProblemPageLocator.PROBLEM_DESCRIPTION)

    def is_description_filled(self):
        """This method return True if description field is filled with data"""
        desc = self.driver.find_element(*AddProblemPageLocator.PROBLEM_DESCRIPTION).get_attribute("value")
        if len(desc) > 0:
            return True
        else:
            return False

    def fill_description_of_problem(self, description):
        """This method fills description field"""
        self.type(description, *AddProblemPageLocator.PROBLEM_DESCRIPTION)

    def is_proposal_filed_present(self):
        """This method return True if proposal field is present"""
        return self.is_element_present(*AddProblemPageLocator.PROPOSAL)

    def fill_proposal_of_solving(self, proposal):
        """This method fills proposal field"""
        self.driver.find_element(*AddProblemPageLocator.PROPOSAL).clear()
        self.type(proposal, *AddProblemPageLocator.PROPOSAL)

    def is_proposal_filled(self):
        """This method return True if proposal field is filled with data"""
        proposal = self.driver.find_element(*AddProblemPageLocator.PROPOSAL).get_attribute("value")
        if len(proposal) > 0:
            return True
        else:
            return False

    def is_next_button_filed_present(self):
        """This method return True if next button is present"""
        return self.is_element_present(*AddProblemPageLocator.NEXT)

    def click_on_next(self):
        """This method clicks on next button"""
        self.click(*AddProblemPageLocator.NEXT)

    def is_publish_button_present(self):
        """This method return True if publish button is present"""
        self.wait_until_element_is_visible(AddProblemPageLocator.PUBLISH)
        return self.is_element_visible(*AddProblemPageLocator.PUBLISH)

    def click_on_publish(self):
        """This method clicks on publish button"""
        self.click(*AddProblemPageLocator.PUBLISH)

    def is_search_button_present(self):
        """This method return True if search button is present"""
        return self.is_element_present(*AddProblemPageLocator.SEARCH)

    def click_on_search(self):
        """This method clicks on search button"""
        self.click(*AddProblemPageLocator.SEARCH)

    def is_add_photo_element_present(self):
        """This method return True if element for uploading photo is present"""
        return self.is_element_present(*AddProblemPageLocator.ADD_PHOTO_ELEMENT)

    def is_description_of_photo_present(self):
        """This method return True if photo description field is present"""
        return self.is_element_present(*AddProblemPageLocator.PHOTO_DESCRIPTION)

    def add_photo_and_description(self, description):
        """Upload photo using path to image from environment variable PYTHONPATH
           and add description to it"""
        input_field = self.driver.find_element(*AddProblemPageLocator.UPLOAD_PHOTO)
        pythonpath = os.environ.get('PYTHONPATH')
        pythonpath1 = pythonpath.lstrip(':')
        input_field.send_keys(pythonpath1 + '/tests/test_img.png')
        self.driver.find_element(*AddProblemPageLocator.PHOTO_DESCRIPTION).clear()
        self.type(description, *AddProblemPageLocator.PHOTO_DESCRIPTION)

    def is_photo_uploaded(self):
        """This method return True if photo is uploaded"""
        return self.is_element_present(*AddProblemPageLocator.CHECK_UPLOADED_PHOTO)

    def get_confirmation_message(self):
        """Return top notification after adding new problem"""
        if self.is_element_present(*AddProblemPageLocator.ERROR_MESSAGE):
            return self.driver.find_element(*AddProblemPageLocator.ERROR_MESSAGE).text
        return self.driver.find_element(*AddProblemPageLocator.CONFIRMATION_MESSAGE).text

    def get_errors_if_coordinates_is_empty(self):
        """Return error messages near latitude and longitude fields"""
        error_messages = list()
        error_messages.append(self.driver.find_element(*AddProblemPageLocator.ERROR_EMPTY_LATITUDE).text)
        error_messages.append(self.driver.find_element(*AddProblemPageLocator.ERROR_EMPTY_LONGITUDE).text)
        return error_messages

    def get_errors_if_incorrect_data_in_fields(self):
        """Return error messages near title, description and proposal fields"""
        errors_messages = list()
        errors_messages.append(self.driver.find_element(*AddProblemPageLocator.TITLE_ERROR).text)
        errors_messages.append(self.driver.find_element(*AddProblemPageLocator.PROPOSAL_ERROR).text)
        errors_messages.append(self.driver.find_element(*AddProblemPageLocator.PROPOSAL_ERROR).text)
        return errors_messages
