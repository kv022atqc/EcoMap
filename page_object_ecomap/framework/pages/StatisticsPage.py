#!/usr/bin/env python
# -*- coding: utf-8 -*-

from framework.Locators import StatisticPageLocator, HomePageLocator
from framework.pages.BasePage import BasePage
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class StatisticPage(BasePage):

    _problems = None
    _subscriptions = None
    _comments = None
    _photos = None

    def get_expected_url(self):
        return StatisticPageLocator.URL

    def goToStatisticPage(self):
        _d = self.driver
        wait = WebDriverWait(_d , 10)
        wait.until(lambda _d: _d.find_element_by_css_selector(HomePageLocator.STATISTIC_PAGE_UNREGISTERED))
        element = _d.find_element_by_css_selector(HomePageLocator.STATISTIC_PAGE_UNREGISTERED)
        element.click()

    def get_subscriptions(self):
        wait = WebDriverWait(self.driver , 10)
        wait.until(
            EC.presence_of_element_located((By.XPATH, StatisticPageLocator.SUBSCRIPTIONS_XPATH) ))
        _subscriptions = int(
            self.driver.find_element_by_css_selector(StatisticPageLocator.SUBSCRIPTIONS_CSS).text)
        return _subscriptions

    def verify_subscription(self):
        _d = self.driver
        wait = WebDriverWait(_d , 10)
        wait.until(lambda _d: _d.find_elements_by_xpath(StatisticPageLocator.SUBSCRIPTIONS_LIST_XPATH))
        elements_in_subscription = _d.find_elements_by_xpath(StatisticPageLocator.SUBSCRIPTIONS_LIST_XPATH)
        return True if len(elements_in_subscription) == self.get_subscriptions() else False

    def get_comments(self):
        wait = WebDriverWait(self.driver , 10)
        wait.until( EC.presence_of_element_located((By.XPATH, StatisticPageLocator.COMMENTS_XPATH)) )
        _comments = int(
            self.driver.find_element_by_css_selector(StatisticPageLocator.COMMENTS_CSS).text)
        return _comments

    def verify_comments(self):
        _d = self.driver
        wait = WebDriverWait(_d , 10)
        if self.get_comments() > 0:
            wait.until(lambda _d: _d.find_elements_by_xpath(StatisticPageLocator.COMMENTS_LIST_XPATH))
            elements_in_commented = _d.find_elements_by_xpath(StatisticPageLocator.COMMENTS_LIST_XPATH)
        else: elements_in_commented = []
        return True if ((len(elements_in_commented) <= self.get_comments())
                         and (len(elements_in_commented) <= 10)) else False

    def get_problems(self):
        wait = WebDriverWait(self.driver , 10)
        wait.until(
            EC.presence_of_element_located((By.XPATH, StatisticPageLocator.PROBLEMS_XPATH)))
        _problems = int(
            self.driver.find_element_by_css_selector(StatisticPageLocator.PROBLEMS_CSS).text)
        return _problems

    def verify_severities(self):
        _d = self.driver
        __problems = self.get_problems()
        wait = WebDriverWait(_d , 10)
        wait.until(lambda _d: _d.find_elements_by_xpath(StatisticPageLocator.IN_SEVERITIES_LIST_XPATH))
        elements_in_severities = _d.find_elements_by_xpath(StatisticPageLocator.IN_SEVERITIES_LIST_XPATH)
        return (__problems == len(elements_in_severities)) if __problems <= 10 else (len(elements_in_severities) == 10)

    def open_top_first_issue(self):
        return self.click(*StatisticPageLocator.TOP_FIRST_ISSUE)
