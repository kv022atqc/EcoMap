#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os
import unittest
from framework.pages.AddProblem import AddProblemPage
from framework.pages.SpikePage import HomeUserPage
from tests.TestBase import TestBase
from framework.Dictionary import DICTIONARY as test_data


class MobileTestLocation(TestBase):

    @classmethod
    def setUpClass(cls):
        super(MobileTestLocation, cls).setUpClass()
        cls.home_user_page = HomeUserPage(cls.driver)
        cls.add_problem = AddProblemPage(cls.driver)

    def test_location(self):

        self.home_page.get_navigation_button()
        self.login_page = self.home_page.get_login_page()
        self.login_page.login(test_data.get("email"), test_data.get("password"))

        # check if "Add problem" tab is present
        self.assertTrue(self.home_user_page.get_current_url(), self.home_user_page.get_expected_url())
        self.assertTrue(self.home_user_page.is_add_problem_tab_present())

        # go to Add problem page
        self.home_user_page.get_navigation_button()
        add_problem = self.home_user_page.click_on_add_problem()
        self.assertEqual(add_problem.get_current_url(), add_problem.get_expected_url())

        # get coordinates by application
        found_coordinates = self.add_problem.click_on_find_me()

        # get actual coordinates from outside service
        actual_coordinates = self.add_problem.get_actual_coordinates()

        self.assertTrue(self.add_problem.check_location(found_coordinates, actual_coordinates),
                        self.add_problem.get_reason_of_fail())

        self.assertTrue(self.add_problem.is_location_widget_present())


if __name__ == '__main__':
    unittest.main()
