#!/usr/bin/env python
# -*- coding: utf-8 -*-

from framework.Locators import HomePageLocator, MapLocator, NavigationLocator
from framework.pages.BasePage import BasePage
from framework.pages.LoginPage import LoginPage
from framework.pages.RegistrationPage import Registration


class HomePage(BasePage):
    def get_login_page(self):
        self.click(*HomePageLocator.LOG_IN)
        return LoginPage(self.driver)

    def get_expected_url(self):
        return HomePageLocator.URL

    def get_registration_page(self):
        self.click(*HomePageLocator.REGISTER)
        return Registration(self.driver)

    def is_login_link_present(self):
        return self.is_element_present(*HomePageLocator.LOG_IN)

    def is_map_present(self):
        self.wait_until_visibility_of_element_located(MapLocator.MAP_IMG, 20)
        return self.is_element_present(*MapLocator.MAP)

    def get_navigation_button(self):
        return self.click(*NavigationLocator.MOBILE_NAVIGATION)
