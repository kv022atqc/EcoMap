#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import unittest
from framework.pages.SpikePage import HomeUserPage, UserProfilePage
from framework.pages.StatisticsPage import StatisticPage
from framework.pages.UserProfileSubscriptionPage import UserProfileSubscriptionPage
from tests.TestBase import TestBase
from framework.Dictionary import DICTIONARY as test_data
from framework.pages.ProblemPage import ProblemPage


class MobileCheckSubscriptionAdmin(TestBase):

    @classmethod
    def setUpClass(cls):
        super(MobileCheckSubscriptionAdmin, cls).setUpClass()
        cls.user_page = HomeUserPage(cls.driver)
        cls.user_profile_page = UserProfilePage(cls.driver)
        cls.user_profile_subscription_page = UserProfileSubscriptionPage(cls.driver)
        cls.statistic = StatisticPage(cls.driver)
        cls.problem_page = ProblemPage(cls.driver)

    def test_check_sub(self):
        self.user_page.get_navigation_button()
        self.login_page = self.home_page.get_login_page()
        self.home_user_page = self.login_page.login(test_data.get("email"), test_data.get("password"))

        self.user_page.get_navigation_button()
        self.user_page.get_user_profile_page()

        # open subscription tab and check subscription
        self.user_profile_subscription_page.mobile_open_subscription_page()

        count = self.user_profile_subscription_page.get_count()

        self.user_page.get_navigation_button()
        self.user_page.get_statistics_page()
        # open top first issue
        self.statistic.open_top_first_issue()

        self.driver.switch_to.window(self.driver.window_handles[1])
        title = self.problem_page.check_title()

        self.problem_page.tap_subscription()
        self.assertTrue(self.problem_page.is_success_popup_present)
        # check correct notification
        self.assertEqual(self.problem_page.get_popup_text(), "Додавання")

        self.user_page.get_navigation_button()
        self.user_page.get_user_profile_page()

        # open subscription tab and check subscription
        self.user_profile_subscription_page.mobile_open_subscription_page()
        self.assertEqual(self.user_profile_subscription_page.get_count(), str(int(count) + 1))

        self.user_page.get_navigation_button()
        self.user_page.get_statistics_page()
        # open top first issue
        self.statistic.open_top_first_issue()

        self.driver.switch_to.window(self.driver.window_handles[2])
        title = self.problem_page.check_title()

        self.problem_page.tap_subscription()
        self.assertTrue(self.problem_page.is_success_popup_present)
        # check correct notification
        self.assertEqual(self.problem_page.get_popup_text(), "Видалення")


if __name__ == '__main__':
    unittest.main()
