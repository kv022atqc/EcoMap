#!/usr/bin/env python
# -*- coding: utf-8 -*-

from framework.Locators import UserProfileProblemsLocator, CommonLocator, UserProfileNavigationLocator, \
    UserProfileSubscriprionLocator
from framework.pages.BasePage import BasePage


class UserProfileSubscriptionPage(BasePage):
    def get_expected_url(self):
        return UserProfileProblemsLocator.URL

    def open_subscription_page(self):
        self.wait_until_invisibility_of_element_located(CommonLocator.SUCCESS_POPUP, timeout=10)
        return self.click(*UserProfileNavigationLocator.SUBSCRIPTIONS_TAB)

    def get_number_subscription(self):
        return self.find_element(*UserProfileSubscriprionLocator.SUBSCRIPTIONS_INFO).text

    def get_title_1(self):
        return self.find_element(*UserProfileSubscriprionLocator.TITLE_1).text

    def get_count(self):
        return self.find_element(*UserProfileSubscriprionLocator.COUNT).text

    def open_view(self):
        return self.click(*UserProfileSubscriprionLocator.VIEW)

    def mobile_open_subscription_page(self):
        self.wait_until_invisibility_of_element_located(CommonLocator.SUCCESS_POPUP, timeout=10)
        return self.click(*UserProfileNavigationLocator.MOBILE_SUBSCRIPTION_TAB)