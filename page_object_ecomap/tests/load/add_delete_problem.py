from locust import HttpLocust, TaskSet, task
from framework.Dictionary import DICTIONARY as TEST_DATA
from requests_toolbelt.multipart.encoder import MultipartEncoder
from framework.Utils import generate_post


class UserBehaviour(TaskSet):

    def on_start(self):
        self.user_id = self.log_in()

    def log_in(self):
        response = self.client.post("/api/login", json={"email": TEST_DATA.get("email"),
                                                        "password": TEST_DATA.get("password")})
        response_json = response.json()
        return response_json["id"]

    def add_problem(self):
        m = MultipartEncoder(fields=generate_post())
        response = self.client.post("/api/problem_post", data=m, headers={'Content-Type': m.content_type})
        response_json = response.json()
        return response_json["problem_id"], response_json["added_problem"]

    def delete_problem(self, problem_id, problem_title):
        response = self.client.delete("/api/problem_delete", json={"problem_id": problem_id,
                                                                   "problem_title": problem_title,
                                                                   "user_id": self.user_id})

    @task
    def order_tests(self):
        problem_id, problem_title = self.add_problem()
        self.delete_problem(problem_id, problem_title)


class WebUser(HttpLocust):
    task_set = UserBehaviour
    min_wait = 5 * 1000
    max_wait = 60 * 1000
