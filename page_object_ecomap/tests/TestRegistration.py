import unittest
from framework.Locators import RegisterPageLocator
from framework.Utils import *
from framework.Dictionary import DICTIONARY as test_data
from framework.pages.RegistrationPage import Registration
from framework.pages.SpikePage import HomeUserPage
from tests.TestBase import TestBase


class TestRegistration(TestBase):
    @classmethod
    def setUpClass(cls):
        super(TestRegistration, cls).setUpClass()
        cls.user_page = HomeUserPage(cls.driver)
        cls.reg_page = Registration(cls.driver)
        cls.home_page.get_registration_page()
        cls.reg_page.wait_until_block_loads()

    def setUp(self):
        self.home_page.get_registration_page()
        self.reg_page.wait_until_block_loads()

    def test_check_registration_page(self):
        self.assertEqual(self.reg_page.get_current_url(), self.reg_page.get_expected_reg_url())
        self.assertTrue(self.reg_page.is_element_present(*RegisterPageLocator.REG_BLOCK))
        self.assertTrue(self.reg_page.is_element_present(*RegisterPageLocator.EMAIL))
        self.assertTrue(self.reg_page.is_element_present(*RegisterPageLocator.NAME))
        self.assertTrue(self.reg_page.is_element_present(*RegisterPageLocator.SURNAME))
        self.assertTrue(self.reg_page.is_element_present(*RegisterPageLocator.NICKNAME))
        self.assertTrue(self.reg_page.is_element_present(*RegisterPageLocator.PASSWORD))
        self.assertTrue(self.reg_page.is_element_present(*RegisterPageLocator.CONFIRMPASSWORD))
        self.assertTrue(self.reg_page.is_element_present(*RegisterPageLocator.SUBMIT_BUTTON))

        self.reg_page.reg(test_data.get("registration_email") % generate_random_number(),
                          test_data.get("registration_name"),
                          test_data.get("registration_surname"),
                          test_data.get("registration_nickname") % generate_random_number(),
                          test_data.get("registration_password"),
                          test_data.get("registration_confirm_password"))

        self.reg_page.wait_linked_text_changed()
        user_name = test_data.get("registration_name") + " " + test_data.get("registration_surname")
        att = self.user_page.user_credentials_btn_is_present()
        self.assertEqual(user_name.upper(), att)
        self.assertTrue(self.user_page.is_logout_btn_present())
        self.user_page.logout()

    def test_check_the_upper_boundary(self):
        self.reg_page.reg(test_data.get("check_email") % generate_random_word(length_of_word=88),
                          test_data.get("check_name") % generate_random_word_without_digits(length_of_word=13),
                          test_data.get("check_surname") % generate_random_word_without_digits(length_of_word=13),
                          test_data.get("check_nickname") % generate_random_word_without_digits(length_of_word=18),
                          test_data.get("check_password") % test_data.get("data_long_40"),
                          test_data.get("check_confirm_password") % test_data.get("data_long_40"))
        self.assertTrue(self.user_page.is_logout_btn_present())
        self.user_page.logout()

    def test_check_the_lower_boundary(self):
        self.reg_page.reg(test_data.get("email_short_6") % generate_random_word(length_of_word=1),
                          generate_random_word_without_digits(length_of_word=2),
                          generate_random_word_without_digits(length_of_word=2),
                          generate_random_word_without_digits(length_of_word=1),
                          test_data.get("data_short_6"),
                          test_data.get("data_short_6"))
        self.assertTrue(self.user_page.is_logout_btn_present())
        self.user_page.logout()

    def test_check_with_empty_fields(self):
        self.reg_page.reg("", "", "", "", "", "")
        self.assertEqual(self.reg_page.get_message_for_email_field(), test_data.get("text_obligatory"))
        self.assertEqual(self.reg_page.get_message_for_name_field(), test_data.get("text_obligatory"))
        self.assertEqual(self.reg_page.get_message_for_surname_field(), test_data.get("text_obligatory"))
        self.assertEqual(self.reg_page.get_message_for_nickname_field(), test_data.get("text_obligatory"))
        self.assertEqual(self.reg_page.get_message_for_password_field(), test_data.get("text_obligatory"))
        self.assertEqual(self.reg_page.get_message_for_confirm_password_field(), test_data.get("text_obligatory"))

    def test_check_the_lower_boundary_minus_one(self):
        self.reg_page.reg(test_data.get("email_short_5"),
                          generate_random_word_without_digits(length_of_word=1),
                          generate_random_word_without_digits(length_of_word=1),
                          generate_random_word_without_digits(length_of_word=1),
                          test_data.get("data_short_5"),
                          test_data.get("data_short_5"))
        self.assertEqual(self.reg_page.get_message_for_email_field(), test_data.get("text_incorrect_email"))
        self.assertEqual(self.reg_page.get_message_for_name_field(), test_data.get("text_incorrect_data"))
        self.assertEqual(self.reg_page.get_message_for_surname_field(), test_data.get("text_incorrect_data"))
        self.assertEqual(self.reg_page.get_message_for_password_field(), test_data.get("text_to_short"))
        self.assertEqual(self.reg_page.get_message_for_confirm_password_field(), test_data.get("text_passwords_not_matches"))

    def test_check_the_upper_boundary_plus_one(self):
        self.reg_page.reg(test_data.get("check_email") % generate_random_word(length_of_word=89),
                          test_data.get("check_name") % generate_random_word_without_digits(length_of_word=14),
                          test_data.get("check_surname") % generate_random_word_without_digits(length_of_word=14),
                          test_data.get("check_nickname") % generate_random_word_without_digits(length_of_word=19),
                          test_data.get("check_password") % test_data.get("data_long_41"),
                          test_data.get("check_confirm_password") % (test_data.get("data_long_41")))
        self.assertEqual(self.reg_page.get_message_for_email_field(), test_data.get("text_to_long"))
        self.assertEqual(self.reg_page.get_message_for_name_field(), test_data.get("text_to_long"))
        self.assertEqual(self.reg_page.get_message_for_surname_field(), test_data.get("text_to_long"))
        self.assertEqual(self.reg_page.get_message_for_nickname_field(), test_data.get("text_to_long"))
        self.assertEqual(self.reg_page.get_message_for_password_field(), test_data.get("text_to_long"))
        self.assertEqual(self.reg_page.get_message_for_confirm_password_field(),test_data.get("text_passwords_not_matches"))

    def test_check_repeats(self):
        self.reg_page.reg(test_data.get("user_for_add_problem"),
                          generate_random_word_without_digits(length_of_word=3),
                          generate_random_word_without_digits(length_of_word=3),
                          test_data.get("anonymous_nickname"),
                          test_data.get("data_short_6"),
                          test_data.get("data_short_5"))
        self.assertEqual(self.reg_page.get_message_for_email_field(), test_data.get('text_the_same_email'))
        self.assertEqual(self.reg_page.get_message_for_nickname_field(), test_data.get('text_the_same_nickname'))
        self.assertEqual(self.reg_page.get_message_for_confirm_password_field(), test_data.get("text_passwords_not_matches"))

    def tearDown(self):
        super(TestRegistration, self).tearDown()
        self.home_page.get_registration_page()

if __name__ == '__main__':
        unittest.main()
