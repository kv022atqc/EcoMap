#!/usr/bin/env python
# -*- coding: utf-8 -*-

from framework.Locators import RegisterPageLocator, HomeUserPageLocator
from framework.pages.BasePage import BasePage
from selenium.webdriver.support.wait import WebDriverWait


class Registration(BasePage):
    def reg(self, email, name, surname, nickname, password, confirmpassword):
        self.type(email, *RegisterPageLocator.EMAIL)
        self.type(name, *RegisterPageLocator.NAME)
        self.type(surname, *RegisterPageLocator.SURNAME)
        self.type(nickname, *RegisterPageLocator.NICKNAME)
        self.type(password, *RegisterPageLocator.PASSWORD)
        self.type(confirmpassword, *RegisterPageLocator.CONFIRMPASSWORD)
        self.click(*RegisterPageLocator.SUBMIT_BUTTON)

    def click_register_button(self):
        return self.click(*RegisterPageLocator.SUBMIT_BUTTON)

    def get_expected_reg_url(self):
        return RegisterPageLocator.REG_URL

    def wait_linked_text_changed(self):
        _driver = self.driver
        WebDriverWait(self.driver, 10).until(lambda _driver: _driver.find_element(*HomeUserPageLocator.USER_CREDENTIALS).text != 'УВІЙТИ')

    def get_message_for_email_field(self):
        return self.find_element(*RegisterPageLocator.MESSAGE_EMAIL).text

    def get_message_for_name_field(self):
        return self.find_element(*RegisterPageLocator.MESSAGE_NAME).text

    def get_message_for_surname_field(self):
        return self.find_element(*RegisterPageLocator.MESSAGE_SURNAME).text

    def get_message_for_nickname_field(self):
        return self.find_element(*RegisterPageLocator.MESSAGE_NICKNAME).text

    def get_message_for_password_field(self):
        return self.find_element(*RegisterPageLocator.MESSAGE_PASSWORD).text

    def get_message_for_confirm_password_field(self):
        return self.find_element(*RegisterPageLocator.MESSAGE_CONFIRM_PASSWORD).text

    def wait_until_block_loads(self):
        return self.is_element_visible(*RegisterPageLocator.REG_BLOCK)