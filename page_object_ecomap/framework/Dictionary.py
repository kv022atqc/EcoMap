#!/usr/bin/env python
# -*- coding: utf-8 -*-

DICTIONARY = {
        'email': 'admin@ecomap.com',
        'password': 'secre!',

        'registration_email': 'user%s@ukr.net',
        'registration_name': 'iamuser',
        'registration_surname': 'ecomap',
        'registration_nickname': 'eco%s',
        'registration_password': 'qwerty',
        'registration_confirm_password': 'qwerty',

        'check_email': 'user%s@ukr.net',
        'check_name': 'iamuser%s',
        'check_surname': 'ecomaps%s',
        'check_nickname': 'ecomaps%s',
        'check_password': 'qwerty%s',
        'check_confirm_password': 'qwerty%s',

        'data_long_40': 'OncewhenIwassixyearsoldIsawamagnif',
        'data_long_41': 'OncewhenIwassixyearsoldIsawamagnifg',
        'data_short_6': 'qwerty',
        'data_short_5': 'qwert',
        'email_short_6': '%s@u.ua',
        'email_short_5': '@u.ua',
        'text_to_long': 'Занадто довге поле.',
        'text_to_short': 'Занадто коротке поле.',
        'text_obligatory': "Це поле є обов'язковим.",
        'text_incorrect_email': "Некоректна електронна пошта.",
        'text_incorrect_data': "Некоректні дані.",
        'text_the_same_email': 'Користувач із такою електронною поштою вже зареєстрований.',
        'text_the_same_nickname': 'Користувач із таким псевдонімом вже зареєстрований.',
        'text_passwords_not_matches': 'Паролі не співпадають.',


        'change_password_email': 'qqq@qqq.com',
        'change_password_password': 'qqqqqq',
        'change_password_new_pass': 'qqqqqq',
        'change_password_repeat_wrong': '654321',

        'new_issue_type': 'Проблеми лісів3',

        'user_for_add_problem': 'anonymous@ecomap.com',
        'password_for_add_problem_user': 'secre!',
        'latitude': '50.4333',
        'longitude': '30.5167',
        'admin_nickname': 'admin',
        'anonymous_nickname': 'anonymous',
        'problem_title': 'test comment2',
        'comment_text': 'comment text 1',
        'answer_text': 'answer text 1'
}


