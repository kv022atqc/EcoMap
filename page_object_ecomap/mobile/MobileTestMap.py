#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import unittest
from framework.Locators import MapLocator
from framework.Screenshot import Screenshot
from framework.pages.SpikePage import HomeUserPage
from tests.TestBase import TestBase
from framework.Dictionary import DICTIONARY as test_data


class TestMapMobile(TestBase):
    @classmethod
    def setUpClass(cls):
        super(TestMapMobile, cls).setUpClass()
        cls.screenshot = Screenshot(cls.driver)
        cls.user_page = HomeUserPage(cls.driver)

    def test_map_displaying(self):
        self.assertEqual(self.driver.title, "Екологічні проблеми України")
        self.assertTrue(self.user_page.is_map_present_mobile(), "Map isn't loaded on Home User page")
        map_img = self.screenshot.get_cropped_image("screen_full.png", "screen_cropped.png", MapLocator.MAP)
        grey_pixels = self.screenshot.get_pixels_by_color('L', 227, map_img)
        white_pixels = self.screenshot.get_pixels_by_color('L', 255, map_img)
        all_pixels = self.screenshot.get_pixels_count(map_img)
        grey_percent = self.screenshot.get_pixels_percentage(grey_pixels, all_pixels)
        white_percent = self.screenshot.get_pixels_percentage(white_pixels, all_pixels)
        self.assertLess(grey_percent, 10, 'Map isn''t displayed properly (too much of grey color)')
        self.assertLess(white_percent, 20, 'Map isn''t displayed properly (too much of white color)')


class TestMapMobileLoggedIn(TestMapMobile):
    def setUp(self):
        self.user_page.get_navigation_button()
        self.home_user_page = self.home_page.get_login_page().login(test_data.get("email"), test_data.get("password"))

    def tearDown(self):
        self.user_page.get_navigation_button()
        self.home_user_page.logout()


if __name__ == '__main__':
    unittest.main()
