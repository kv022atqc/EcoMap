#!/usr/bin/env python
# -*- coding: utf-8 -*-

from framework.Locators import *
from framework.pages.BasePage import BasePage
from framework.pages.EditProblemPage import EditProblemPage
from framework.pages.ProblemPage import ProblemPage, NotSubmittedProblemPage
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from retry import retry


class UserProfileProblemsPage(BasePage):
    """Tab in User's profile with all problems created by user for User and with all problems in database for Admin"""

    @retry(tries=8, delay=2)
    def get_edit_problem_page(self):
        self.wait_until_invisibility_of_element_located(CommonLocator.SUCCESS_POPUP, timeout=10)
        problems_count = len(self.driver.find_elements(*ProblemsUserProfileLocator.PROBLEMS_LIST))
        if problems_count > 0:
            self.click(*ProblemsUserProfileLocator.LAST_PROBLEM_EDIT_LINK)
            return EditProblemPage(self.driver)
        else:
            return None

    def get_expected_url(self):
        """:return expected url of the page"""
        return self.base_url + UserProfileProblemsLocator.URL

    def is_table_present(self):
        """:returns true if table with problems is present, false otherwise"""
        return self.is_element_present(*UserProfileProblemsLocator.TABLE)

    def get_problem(self, is_submitted):
        """
        :param is_submitted: True - get submitted problem, False - otherwise
        :return problem of type UserProfileProblemsPage.TableRow
        where problem submit status is according to input
        """
        index, page = self.__get_problem_index__(is_submitted)
        if index == -1:
            return None
        problem_locator = self.__create_table_raw_locator__(index)
        return self.__create_problem__(page, problem_locator)

    def get_specific_problem(self, number):
        locator = self.__create_locator__(UserProfileProblemsLocator.SPECIFIC_PROBLEM_EDIT_LINK.__getitem__(1), number)
        self.click(*locator)

    def __get_problem_index__(self, is_submitted):
        """returns index and current page of first problem with appropriate status"""
        index = -1
        amount_of_pages = int(int(self.get_total_amount_of_problems()) / self.get_amount_of_problems_per_page())
        for page in range(0, amount_of_pages):
            index = self.__get_problem_index_on_table_page__(is_submitted)
            if index != -1:
                return index, page
            self.get_next_problems()
        return index, 0

    def __get_problem_index_on_table_page__(self, is_submitted):
        """returns index on current page of first problem with appropriate status"""
        amount_of_problems_on_page = self.get_amount_of_problems_per_page()
        for i in range(1, amount_of_problems_on_page + 1):
            SUBMIT_STATUS_LOCATOR = self.__create_locator__(UserProfileProblemsLocator.TableRowLocator.SUBMIT_STATUS, i)
            try:
                if self.__is_problem_submitted__(SUBMIT_STATUS_LOCATOR) == is_submitted:
                    return i
            except Exception:
                return -1
        return -1

    def __create_table_raw_locator__(self, index):
        """dynamically create locator of table row according to input row number
        :param index: (int) number of row in the table
        :return UserProfileProblemsLocator.TableRowLocator object with locators to all elements in the row"""
        locator = UserProfileProblemsLocator.TableRowLocator()
        locator.SUBMIT_STATUS = self.__create_locator__(locator.SUBMIT_STATUS, index)
        locator.SUBMIT_STATUS_MESSAGE = self.__create_locator__(locator.SUBMIT_STATUS_MESSAGE, index)
        locator.USER_NAME = self.__create_locator__(locator.USER_NAME, index)
        locator.USER_NICKNAME = self.__create_locator__(locator.USER_NICKNAME, index)
        locator.HEADER =  self.__create_locator__(locator.HEADER, index)
        locator.PROBLEM_TYPE = self.__create_locator__(locator.PROBLEM_TYPE, index)
        locator.PROBLEM_STATUS = self.__create_locator__(locator.PROBLEM_STATUS, index)
        locator.CREATION_DATE = self.__create_locator__(locator.CREATION_DATE, index)
        locator.SHOW_PROBLEM_LINK = self.__create_locator__(locator.SHOW_PROBLEM_LINK, index)
        locator.EDIT_PROBLEM_LINK = self.__create_locator__(locator.EDIT_PROBLEM_LINK, index)
        locator.DELETE_PROBLEM_LINK = self.__create_locator__(locator.DELETE_PROBLEM_LINK, index)
        return locator

    def __create_problem__(self, page, problem_locator):
        """:return UserProfilePage.TableRow object with locator of problem in table
        and table page there the row is located"""
        problem = self.TableRow(page=page, locator=problem_locator)
        problem.header = self.find_element(*problem.locator.HEADER).text
        problem.user = self.find_element(*problem.locator.USER_NAME).text
        return problem

    @staticmethod
    def __create_locator__(xpath, raw):
        """create XPATH locator"""
        return (By.XPATH, xpath % raw)

    def click_edit_problem(self, problem):
        """finds problem in table and clicks 'edit' button in the problem row
        :param problem: UserProfilePage.TableRow object
        :returns page according to submit status of problem (ProblemPage if submitted,
        NotSubmittedProblemPage otherwise)"""
        self.go_to_table_page(problem.page)
        if self.is_problem_submitted(problem):
            self.click(*problem.locator.EDIT_PROBLEM_LINK)
            return ProblemPage(self.driver)
        else:
            self.click(*problem.locator.EDIT_PROBLEM_LINK)
            return NotSubmittedProblemPage(self.driver)

    def edit_first_problem(self):
        self.click(*UserProfileProblemsLocator.FIRST_PROBLEM_EDIT_LINK)
        return ProblemPage(self.driver)

    def is_problem_submitted(self, problem):
        """previously you need to find page where the problem is located
        :param problem: UserProfilePage.TableRow object
        :return True if problem in problems' table is submitted, False otherwise"""
        return self.__is_problem_submitted__(problem.locator.SUBMIT_STATUS)

    def __is_problem_submitted__(self, submit_status_locator):
        """:return True if problem in problems' table is submitted, False otherwise"""
        problem_submit_status = self.find_element(*submit_status_locator)
        return not ('ng-hide' in problem_submit_status.get_attribute("class"))

    def count_not_submitted_problems(self):
        """:return amount of not submitted problems in the problems' table"""
        amount_of_pages = self.get_amount_of_pages()
        amount_of_not_submitted_problems = 0
        for page in range(0, amount_of_pages):
            for row in range(1, self.get_actual_amount_of_problems_on_page() + 1):
                SUBMIT_STATUS_LOCATOR = self.__create_locator__(UserProfileProblemsLocator.TableRowLocator.SUBMIT_STATUS, row)
                if not self.__is_problem_submitted__(SUBMIT_STATUS_LOCATOR):
                    amount_of_not_submitted_problems += 1
                else:
                    return amount_of_not_submitted_problems
            self.get_next_problems()
        return amount_of_not_submitted_problems

    def get_problem_status(self, problem):
        """
        :param problem: UserProfilePage.TableRow object
        :return: submit status text in the table's cell
        """
        self.go_to_table_page(problem.page)
        print(self.is_element_present(*problem.locator.PROBLEM_STATUS))
        return self.find_element(*problem.locator.PROBLEM_STATUS).text

    def get_amount_of_problems_per_page(self):
        """
        :return: current amount of problems per page listed in table
        """
        select = Select(self.find_element(*UserProfileProblemsLocator.PROBLEMS_PER_PAGE_DROP_DOWN))
        option = select.first_selected_option
        return int(option.text)

    def get_actual_amount_of_problems_on_page(self):
        """
        :return: actual amount of problems on page
        """
        amount_of_problems = self.get_amount_of_problems_per_page()
        for i in range(1, amount_of_problems + 1):
            locator = self.__create_locator__(UserProfileProblemsLocator.TableRowLocator.HEADER, i)
            if not self.is_element_present(*locator):
                return i
        return amount_of_problems

    def get_amount_of_pages(self):
        """
        :return: amount of table pages
        """
        return int(int(self.get_total_amount_of_problems()) / self.get_amount_of_problems_per_page())

    def get_total_amount_of_problems(self):
        """
        :return: amount of all problems in the table
        """
        try:
            self.wait_until_element_is_visible(UserProfileProblemsLocator.TOTAL_AMOUNT_OF_PROBLEMS)
            return self.find_element(*UserProfileProblemsLocator.TOTAL_AMOUNT_OF_PROBLEMS).text
        except AssertionError:
            return 0

    def go_to_table_page(self, page):
        """:param page: number of page"""
        for i in range(0, page):
            self.get_next_problems()

    def get_next_problems(self):
        """click 'Next' button"""
        self.click(*UserProfileProblemsLocator.SECOND_TABLE_PAGE_BUTTON)

    def get_first_problems(self):
        """click 'First' button - the first table page is open"""
        self.click(*UserProfileProblemsLocator.FIRST_TABLE_PAGE_BUTTON)

    def get_last_problems(self):
        """click 'Last' button - the last table page is open"""
        self.click(*UserProfileProblemsLocator.LAST_TABLE_PAGE_BUTTON)

    class TableRow:
        """class for interacting with elements in table's row"""
        def __init__(self, page, locator):
            """
            :param page: number of table page with page count '5' where the problem row is located
            :param locator: UserProfileProblemsLocator.TableRowLocator object with locators to all elements in the row
            """
            self.page = page
            self.locator = locator
            self.header = None
            self.user = None


    def choose_problem(self):
        self.click(*UserProfileProblemsLocator.PROBLEM_TYPE_DROPDOWN)
        self.click(*UserProfileProblemsLocator.GARBAGE_DUMP_TYPE)
        self.click(*UserProfileProblemsLocator.SUBMIT_BUTTON)
        self.click(*UserProfileProblemsLocator.EDIT_SUBMIT_BUTTON)

    def is_success_popup_present(self):
        """
        :return: True if the pop-up is present, False otherwise
        """
        _d = self.driver
        try:
            WebDriverWait(_d, 5).until(lambda _d: _d.find_element(*UserProfileProblemsLocator.TYPE_CHANGED_SUCCESS_POPUP))
        except Exception:
            return False
        return True
