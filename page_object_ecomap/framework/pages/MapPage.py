#!/usr/bin/env python
# -*- coding: utf-8 -*-

from framework.Locators import MapItemsLocator
from framework.pages.BasePage import BasePage
from framework.pages.HomePage import HomePage
from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class MapFrame(HomePage):
    def zoom_in(self):
        _d = self.driver
        _d.execute_script("_zoom_value = instance.mapInstance.getZoom(); instance.mapInstance.setZoom(_zoom_value+1);")

    def zoom_out(self):
        _d = self.driver
        _d.execute_script("_zoom_value = instance.mapInstance.getZoom(); instance.mapInstance.setZoom(_zoom_value-1);")

    def zoom_to_value(self, zoom_value):
        _d = self.driver
        _zoom_value = _d.execute_script("return instance.mapInstance.getZoom();")
        while _zoom_value != zoom_value:
            if _zoom_value < zoom_value:
                self.zoom_in()
                _zoom_value += 1
            else:
                self.zoom_out()
                _zoom_value -= 1

    def get_number_of_issues_on_map(self):
        _d = self.driver
        _problems = _d.execute_script("return instance.markers.length;")
        return _problems

    def is_issues_marker_displays_correct_value(self):
        _d = self.driver
        self.zoom_to_value(3)
        _d.implicitly_wait(10)
        _text = _d.find_element(*MapItemsLocator.ALL_ISSUES).text
        _problems = self.get_number_of_issues_on_map()
        return (True if int(_text) == _problems else False)

    def is_issues_markers_displays_at_zoom_value(self, zoom_value):
        _d = self.driver
        _d.implicitly_wait(10)
        coord_arr = {}
        _result = True
        coord_arr = _d.execute_script("return instance.markers.map(function(marker)"+
                                      "{return {lat : marker.position.lat(), lng : marker.position.lng()};});")
        #loop coord_arr
            #panto coordinate
            #get bounds
            #calculate issues number in bounds
            #query xpath; calculate actual number
            #assert equality
        self.zoom_to_value(zoom_value)
        for i in range (0, len(coord_arr)):
            _d.execute_script("instance.mapInstance.panTo("+str(coord_arr[i])+");")
            markers_in_bounds = None
            _d.implicitly_wait(10)
            markers_in_bounds = _d.execute_script("var bounds = instance.mapInstance.getBounds();" +
                             "bounds = instance.cluster.getExtendedBounds(bounds);"+
                             "var coordinates = instance.markers.map(function(marker)"+
                             "{return {lat : marker.position.lat(), lng : marker.position.lng()};});"+
                             "return coordinates.reduce(function(number_in_bounds, coord)"+
                             "{return (bounds.contains(coord) ? number_in_bounds+1 : number_in_bounds)},0)")
            _number_of_visible_markers = 0
            _visible_markers = {}
            self.zoom_in()
            self.zoom_out()
            _d.implicitly_wait(10)
            _visible_markers = _d.find_elements_by_xpath('//div[@class="gm-style"]/div[1]/div[4]/div[3]/div')
            for j in range (0, len(_visible_markers)):
                _number_of_visible_markers += 1 if (_visible_markers[j].text == "") else int(_visible_markers[j].text)
            if (markers_in_bounds != _number_of_visible_markers): _result = False
        return _result


class MapPage(BasePage):
    def get_on_problem(self):
        men_menu = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='map']/div/div/div[1]/div[3]/div")))
        ActionChains(self.driver).move_to_element(men_menu).perform()
        ActionChains(self.driver).click(men_menu)
        return self.click(*MapItemsLocator.PROBLEM_ON_MAP)

    def click_on_change_button(self):
        return self.click(*MapItemsLocator.CHANGE_BUTTON)

    def is_success_popup_present(self):
        _d = self.driver
        try:
            WebDriverWait(_d, 5).until(lambda _d: _d.find_element(*MapItemsLocator.SUCCESS_POPUP))
        except Exception:
            return False
        return True