import requests
from ddt import ddt, data
from framework.Dictionary import DICTIONARY as test_data
from framework.Locators import BASE_URL
from framework.Utils import generate_post
from framework.pages.ProblemPage import ProblemPage
from framework.pages.SpikePage import UserProfileProblemsPage
from tests.TestBase import TestBase


@ddt
class TestEditProblemAdmin(TestBase):

    @classmethod
    def setUpClass(cls):
        super(TestEditProblemAdmin, cls).setUpClass()
        login_page = cls.home_page.get_login_page()
        cls.home_user_page = login_page.login(test_data.get('email'), test_data.get('password'))
        url = BASE_URL + '/api/problem_post'
        requests.post(url=url, data=generate_post())

    def setUp(self):
        self.user_profile_page = self.home_user_page.get_user_profile_page()
        self.user_problems_page = UserProfileProblemsPage(self.driver)
        self.problem_page = ProblemPage(self.driver)
        self.selected_problem = None
        self.new_importance = None
        self.new_status = None

    @data(True, False)
    def test_check_problem_edit(self, is_submitted):
        problem = self.select_problem(is_submitted)
        self.assertNotEqual(problem, None)
        self.edit_problem(problem)
        self.assertTrue(self.problem_page.is_success_popup_present())
        self.assertEqual(self.new_importance, self.problem_page.get_current_importance_info())
        self.assertEqual(self.new_status, self.problem_page.get_current_status_info())
        self.go_to_user_problem_page()
        self.assertEqual(self.new_status, self.user_problems_page.get_problem_status(problem))

    def select_problem(self, is_submitted):
        self.user_profile_page.wait_until_page_is_loaded()
        self.user_problems_page = self.user_profile_page.get_problems_page()
        self.assertTrue(self.user_problems_page.is_table_present())
        return self.user_problems_page.get_problem(is_submitted)

    def edit_problem(self, problem):
        user_problems_page = UserProfileProblemsPage(self.driver)
        self.problem_page = user_problems_page.click_edit_problem(problem)
        self.assertTrue(self.problem_page.is_importance_field_present() and
                        self.problem_page.is_status_field_present() and
                        self.problem_page.is_change_button_present())
        self.edit_importance()
        self.edit_status()
        self.problem_page.submit_change()

    def edit_importance(self):
        old_importance = self.problem_page.get_importance()
        self.new_importance = self.problem_page.get_another_importance_from_options(old_importance)
        self.problem_page.change_importance(self.new_importance)
        self.assertNotEqual(old_importance, self.problem_page.get_importance())

    def edit_status(self):
        old_status = self.problem_page.get_status()
        self.new_status = self.problem_page.get_another_status_from_options(old_status)
        self.problem_page.change_status(self.new_status)
        self.assertNotEqual(old_status, self.problem_page.get_status())

    def go_to_user_problem_page(self):
        self.problem_page.go_to_user_profile_problems_page()

    def test_submit_problem(self):
        problem = self.select_problem(False)
        self.assertNotEqual(problem, None)
        amount_of_not_submitted_problems = self.user_problems_page.count_not_submitted_problems()
        self.submit_problem(problem)
        self.assertTrue(self.problem_page.is_success_popup_present())
        self.assertFalse(self.problem_page.is_comment_field_present())
        self.go_to_user_problem_page()
        # because there can be duplicates in table we can detect problem
        # is submitted by comparing amount of not submitted problems
        self.assertNotEqual(amount_of_not_submitted_problems, self.user_problems_page.count_not_submitted_problems())

    def submit_problem(self, problem):
        self.user_problems_page.get_first_problems()
        self.problem_page = self.user_problems_page.click_edit_problem(problem)
        self.assertTrue(self.problem_page.is_importance_field_present() and
                        self.problem_page.is_status_field_present() and
                        self.problem_page.is_submit_field_present() and
                        self.problem_page.is_comment_field_present() and
                        self.problem_page.is_change_button_present())
        self.edit_submit_status()
        self.problem_page.submit_change()

    def edit_submit_status(self):
        old_status = self.problem_page.get_submit_status()
        self.new_status = self.problem_page.get_another_submit_status_from_options(old_status)
        self.problem_page.change_submit_status(self.new_status)
        self.assertNotEqual(old_status, self.problem_page.get_submit_status())

    def tearDown(self):
        super(TestEditProblemAdmin, self).tearDown()
        self.home_page.open()
