#!/usr/bin/env python
# -*- coding: utf-8 -*-

from framework.Locators import ProblemLocator, MapLocator, StatisticPageLocator, NotSubmittedProblemLocator, UserProfileProblemsLocator
from framework.pages.BasePage import BasePage
from selenium.webdriver.support.select import Select


class ProblemPage(BasePage):
    """Class for for interacting with elements on problem page"""
    def is_importance_field_present(self):
        """:returns true if importance drop down list is present, otherwise - false"""
        if self.is_element_present(*ProblemLocator.IMPORTANCE_DROP_DOWN):
            return True
        return False

    def is_status_field_present(self):
        """:returns true if status drop down list is present, otherwise - false"""
        if self.is_element_present(*ProblemLocator.STATUS_DROP_DOWN):
            return True
        return False

    def is_change_button_present(self):
        """:returns true if 'Change' button is present, otherwise - false"""
        if self.is_element_present(*ProblemLocator.CHANGE_BTN):
            return True
        return False

    def change_importance(self, value):
        """sets importance value in importance drop down list"""
        select = Select(self.find_element(*ProblemLocator.IMPORTANCE_DROP_DOWN))
        select.select_by_visible_text(value)

    def change_status(self, status):
        """sets status value in status drop down list"""
        select = Select(self.find_element(*ProblemLocator.STATUS_DROP_DOWN))
        select.select_by_visible_text(status)

    def submit_change(self):
        """clicks 'Change' button"""
        self.click(*ProblemLocator.CHANGE_BTN)

    def is_success_popup_present(self):
        """:returns true if pop-up with successful change message is present, false otherwise"""
        self.wait_until_visibility_of_element_located(ProblemLocator.POP_UP_WINDOW_SUCCESSFUL_CHANGE)
        message = self.find_element(*ProblemLocator.POP_UP_WINDOW_MESSAGE).text
        return 'Редагування статусів здійснено успішно' in message

    def get_popup_text(self):
        """:return pop-up message text"""
        return self.find_element(*ProblemLocator.POP_UP_WINDOW_TITLE).text

    def get_importance(self):
        """:return current importance value in importance drop down list"""
        select = Select(self.find_element(*ProblemLocator.IMPORTANCE_DROP_DOWN))
        option = select.first_selected_option
        return option.text

    def get_status(self):
        """:return current status value in the status drop down list"""
        select = Select(self.find_element(*ProblemLocator.STATUS_DROP_DOWN))
        option = select.first_selected_option
        return option.text

    def get_another_importance_from_options(self, value):
        """:return first value from importance drop down list options which is not equal to input value"""
        return self.__get_another_value_from_options__(ProblemLocator.IMPORTANCE_DROP_DOWN, value)

    def get_another_status_from_options(self, old_status):
        """:return first value from status drop down list options which is not equal to input value"""
        return self.__get_another_value_from_options__(ProblemLocator.STATUS_DROP_DOWN, old_status)

    def __get_another_value_from_options__(self, locator, old_value):
        my_select = Select(self.find_element(*locator))
        for i in range(len(my_select.options)):
            if old_value != my_select.options[i].text:
                return my_select.options[i].text
        return ""

    def get_current_importance_info(self):
        """:return importance value from label with problem importance information"""
        info = self.find_element(*ProblemLocator.IMPORTANCE_INFO).text
        return info.split(' ', 1)[0]

    def get_current_status_info(self):
        """:return status value from label with problem status information"""
        return self.find_element(*ProblemLocator.STATUS_INFO).text

    def tap_subscription(self):
        """subscribe/unsubscribe to/from problem"""
        return self.click(*StatisticPageLocator.EYE)

    def check_title(self):
        """:return problem title"""
        return self.find_element(*ProblemLocator.DETAILED_TITLE).text

    def is_loaded(self):
        """:return true if the map is loaded, false otherwise"""
        return self.is_element_visible(MapLocator.MAP)

    def go_to_user_profile_problems_page(self):
        """opens 'Problems' tab in user profile by url"""
        self.open(UserProfileProblemsLocator.URL)


class NotSubmittedProblemPage(ProblemPage):
    """Class for for interacting with elements on not submitted problem page"""

    def is_submit_field_present(self):
        """:return True if submit drop down list is present, False otherwise"""
        return self.is_element_present(*NotSubmittedProblemLocator.SUBMIT_STATUS_DROP_DOWN)

    def is_comment_field_present(self):
        """:return True if comment field is present, False otherwise"""
        return self.is_element_present(*NotSubmittedProblemLocator.COMMENT_FIELD)

    def change_submit_status(self, status):
        """
        select submit status in submit drop down list
        :param status: status text value
        """
        select = Select(self.find_element(*NotSubmittedProblemLocator.SUBMIT_STATUS_DROP_DOWN))
        select.select_by_visible_text(status)

    def get_submit_status(self):
        """
        :return: currently selected submit status
        """
        select = Select(self.find_element(*NotSubmittedProblemLocator.SUBMIT_STATUS_DROP_DOWN))
        return select.first_selected_option.text

    def get_another_submit_status_from_options(self, old_status):
        """
        :param old_status: status text value
        :return: selected submit status  which is not equal to old_status from options in status drop down list
        """
        return self.__get_another_value_from_options__(NotSubmittedProblemLocator.SUBMIT_STATUS_DROP_DOWN, old_status)

    def leave_comment(self, text):
        """types text into comment filed"""
        self.type(text, *NotSubmittedProblemLocator.COMMENT_FIELD)

    def submit_change(self):
        """click on 'Change' button"""
        self.click(*NotSubmittedProblemLocator.CHANGE_BTN)
