from framework.pages.StatisticsPage import StatisticPage
from tests.TestBase import TestBase

class TestStatistic(TestBase):

    @classmethod
    def setUpClass(cls):
        super(TestStatistic, cls).setUpClass()
        cls.statistic_page = StatisticPage(cls.driver)


    def test_1_assert_statistic_page_is_open(self):
        self.statistic_page.goToStatisticPage()
        self.assertEqual(self.statistic_page.get_expected_url(), self.statistic_page.get_current_url())

    def test_2_assert_valid_stat_in_all_top(self):

        self.assertTrue(self.statistic_page.verify_subscription())
        self.assertTrue(self.statistic_page.verify_severities())
        self.assertTrue(self.statistic_page.verify_comments())

    

