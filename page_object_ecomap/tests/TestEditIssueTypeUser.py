import unittest

from framework.pages.HomePage import HomePage
from framework.pages.LoginPage import LoginPage
from framework.pages.SpikePage import HomeUserPage, UserProfilePage
from framework.pages.UserProfileProblemsPage import UserProfileProblemsPage
from tests.AddProblemForAbstractUser import AddProblem
from framework.Utils import generate_random_word
from tests.TestBase import TestBase
from framework.Dictionary import DICTIONARY as test_data
from framework.Utils import generate_random_number


class TestEditIssueTypeUser(AddProblem):
    @classmethod
    def setUpClass(cls):
        super(TestEditIssueTypeUser, cls).setUpClass()
        cls.home_page = HomePage(cls.driver)
        cls.user_page = HomeUserPage(cls.driver)
        cls.login_page = LoginPage(cls.driver)
        cls.user_profile_page = UserProfilePage(cls.driver)
        cls.user_problems_page = UserProfileProblemsPage(cls.driver)
        cls.home_user_page = cls.login_page.login(
            test_data.get('user_for_add_problem'), test_data.get('password_for_add_problem_user'))

    def test_user_changes_issue_type(self):
        self.go_to_add_problem_page()
        self.locate_problem_with_find_me()
        self.fill_necessary_fields()
        self.publish_problem()
        self.home_user_page.get_user_profile_page().get_problems_page()
        self.user_problems_page.get_specific_problem(1)
        self.user_problems_page.choose_problem()
        self.assertTrue(self.user_problems_page.is_success_popup_present())
