import base64
import os
from selenium import webdriver
from appium import webdriver as appiumWebdriver
from selenium.webdriver import remote
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


BROWSERS = {
    "chrome": DesiredCapabilities.CHROME,
    "firefox": DesiredCapabilities.FIREFOX,
    "phantomjs": DesiredCapabilities.PHANTOMJS
}


class Driver:

    @staticmethod
    def get_driver():
        driver = None
        browser = os.environ.get('SELENIUM_BROWSER','chrome')
        if os.environ.get('SELENIUM_CONNECTION') == 'LOCAL':
            path = os.environ.get('SELENIUM_DRIVER_PATH')
            if browser == 'chrome':
                driver = webdriver.Chrome(executable_path=path)
            if browser == 'firefox':
                driver = webdriver.Firefox(firefox_binary=os.getenv('SELENIUM_FIREFOX_PATH', "/usr/bin/firefox"), executable_path=path)
            if browser == 'phantomjs':
                driver = webdriver.PhantomJS(executable_path=path)
        elif os.environ.get('SELENIUM_CONNECTION') == 'REMOTE':
            if browser == 'chrome':
                options = webdriver.ChromeOptions()
                options.add_argument("--no-sandbox")
                driver = webdriver.Remote(
                    command_executor=os.environ.get('SELENIUM_RC_URL'),
                    desired_capabilities=options.to_capabilities())
            else:
                driver = webdriver.Remote(
                    command_executor=os.environ.get('SELENIUM_RC_URL'),
                    desired_capabilities=BROWSERS[browser])
        elif os.environ.get('SELENIUM_CONNECTION') == 'MOBILE':
            desired_caps = {}
            desired_caps['platformName'] = os.environ.get('PLATFORM_NAME')
            desired_caps['platformVersion'] = os.environ.get('PLATFORM_VERSION')
            desired_caps['deviceName'] = 'Android Emulator'
            desired_caps['browserName'] = 'Chrome'
            driver = appiumWebdriver.Remote(os.environ.get('SELENIUM_RC_URL'), desired_caps)
        return Driver.add_driver_settings(driver)


    @staticmethod
    def add_driver_settings(driver):
        if os.environ.get('SELENIUM_CONNECTION') == 'LOCAL' or os.environ.get('SELENIUM_CONNECTION') == 'REMOTE':
            driver.implicitly_wait(20)
            driver.set_page_load_timeout(30)
            driver.set_window_size(1280, 1024)
        if os.environ.get('SELENIUM_CONNECTION') == 'MOBILE':
            driver.implicitly_wait(50)
            driver.set_page_load_timeout(40)
        return driver

    @staticmethod
    def screenshot(driver, filename):
        if isinstance(driver, remote.webdriver.WebDriver):
            # Get Screenshot over the wire as base64
            base64_data = driver.get_screenshot_as_base64()
            screenshot_data = base64.decodebytes(bytes(base64_data, "utf-8"))
            screenshot_file = open(filename, "wb")
            screenshot_file.write(screenshot_data)
            screenshot_file.close()
        else:
            driver.save_screenshot(filename)

    @staticmethod
    def mobile_remove_popup(_driver):
        if os.environ.get('SELENIUM_CONNECTION') == 'MOBILE':
            _driver.switch_to.context('NATIVE_APP')
            _driver.implicitly_wait(10)
            try:
                _driver.find_element_by_id('terms_accept').click()
                _driver.find_element_by_id('negative_button').click()
            except Exception: print('There was no login popup')
            finally:
                _driver.switch_to.context('CHROMIUM')
                _driver.implicitly_wait(50)
