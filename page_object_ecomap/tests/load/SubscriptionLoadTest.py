from locust import HttpLocust, TaskSet, task
from framework.Dictionary import DICTIONARY as TEST_DATA
from framework.Utils import generate_one_digit_number as RANDOM


class UserBehaviour(TaskSet):
    def on_start(self):
        with self.client.post("/api/login", json={"email": TEST_DATA.get("user_for_add_problem"),
                                                    "password": TEST_DATA.get("password_for_add_problem_user")},
                                  catch_response=True) as response:
            if response.status_code != 200:
                response.failure("login fail")

    @task(10)
    def go_to_map(self):
        self.client.get("/#/map")

    @task(10)
    def go_to_subscriptions(self):
        self.client.get("/#/user_profile/subscriptions")

    @task(10)
    def go_to_statistic(self):
        self.client.get("/#/statistic")

    @task(10)
    def go_to_first_problem(self):
        self.client.get("/#/detailedProblem/1")

    @task(10)
    def make_subscription(self):
        self.client.post("/api/subscription_post", json={"problem_id":RANDOM()})

    @task(10)
    def make_and_delete_subscription1(self):
        id = RANDOM()
        aaa = "/api/subscription_delete?problem_id=" + id
        self.client.post("/api/subscription_post", json={"problem_id": id})
        self.client.delete(aaa)


class WebUser(HttpLocust):
    task_set = UserBehaviour
    min_wait = 5 * 1000
    max_wait = 60 * 1000

