#!/usr/bin/env python
# -*- coding: utf-8 -*-

from framework.Locators import AdministerTabLocator
from framework.pages.BasePage import BasePage
from selenium.webdriver.support.wait import WebDriverWait


class AdministerTabPage(BasePage):
    def get_issue_type_tab(self):
        self.click(*AdministerTabLocator.ISSUE_TYPE_TAB)

    def click_first_issue_changetype_button(self):
        self.click(*AdministerTabLocator.FIRST_ISSUE_CHANGE_STATUS_BUTTON)

    def change_issue_type(self, new_type):
        self.wait_until_element_is_visible(AdministerTabLocator.ISSUE_TYPE_FIELD, timeout=10)
        self.type(new_type, *AdministerTabLocator.ISSUE_TYPE_FIELD)
        self.wait_until_element_is_visible(AdministerTabLocator.SUBMIT_BUTTON, timeout=10)
        self.click(*AdministerTabLocator.SUBMIT_BUTTON)

    def is_success_popup_present(self):
        _d = self.driver
        try:
            WebDriverWait(_d, 5).until(lambda _d: _d.find_element(*AdministerTabLocator.TYPE_CHANGED_SUCCES_POPUP))
        except Exception:
            return False
        return True