import unittest

import os
from framework.Locators import MapItemsLocator
from framework.pages.MapPage import MapFrame
from tests.TestBase import TestBase
import requests


class TestZoom(TestBase):
    @classmethod
    def setUpClass(cls):
        super(TestZoom, cls).setUpClass()
        cls.e_map = MapFrame(cls.driver)

    @unittest.skipIf(os.environ.get('SELENIUM_CONNECTION') == 'REMOTE', 'chrome not reachable on CI')
    def test_zoom(self):
        self.assertTrue(self.e_map.is_issues_marker_displays_correct_value())

    @unittest.skipIf(os.environ.get('SELENIUM_CONNECTION') == 'REMOTE', 'chrome not reachable on CI')
    def test_move_to_coord(self):
        self.assertTrue(self.e_map.is_issues_markers_displays_at_zoom_value(15))
        self.assertTrue(self.e_map.is_issues_markers_displays_at_zoom_value(18))

    def test_clusterisation(self):
        r = requests.get(MapItemsLocator.CLUSTER_IMAGE)
        self.assertTrue(int(r.status_code) == 200)
