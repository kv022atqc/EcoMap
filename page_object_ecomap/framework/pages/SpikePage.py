#!/usr/bin/env python
# -*- coding: utf-8 -*-

from framework.Locators import HomeUserPageLocator, NavigationLocator, MapLocator
from framework.pages.AddProblem import AddProblemPage
from framework.pages.BasePage import BasePage


class HomeUserPage(BasePage):

    def get_expected_url(self):
        return HomeUserPageLocator.URL

    def is_logout_btn_present(self):
        return self.is_element_present(*HomeUserPageLocator.LOGOUT_LINK)

    def user_credentials_btn_is_present(self):
        return self.find_element(*HomeUserPageLocator.USER_CREDENTIALS).text

    def is_user_profile_link_present(self):
        return self.is_element_present(*HomeUserPageLocator.USER_PROFILE_LINK)

    def click_on_add_problem(self):
        self.click(*NavigationLocator.ADD_PROBLEM)
        return AddProblemPage(self.driver)

    def is_add_problem_tab_present(self):
        return self.is_element_present(*NavigationLocator.ADD_PROBLEM)

    def get_user_profile_page(self):
        if self.is_element_present(*HomeUserPageLocator.USER_PROFILE_LINK):
            self.click(*HomeUserPageLocator.USER_PROFILE_LINK)
            return UserProfilePage(self.driver)
        else:
            return None

    def get_statistics_page(self):
        return self.click(*NavigationLocator.STATISTIC)

    def logout(self):
        return self.click(*HomeUserPageLocator.LOGOUT_LINK)

    def get_navigation_button(self):
        return self.click(*NavigationLocator.MOBILE_NAVIGATION)

    def is_map_present_mobile(self):
        self.wait_until_visibility_of_element_located(MapLocator.MAP_IMG_MOBILE, 40)
        return self.is_element_present(*MapLocator.MAP)

from framework.Locators import UserProfileLocator, ProblemsUserProfileLocator, CommentsUserProfileLocator, \
    UserProfileNavigationLocator, UserProfileProblemsLocator
from framework.pages.BasePage import BasePage
from framework.pages.CommentsUserProfilePage import CommentsUserProfilePage
from framework.pages.UserProfileProblemsPage import UserProfileProblemsPage
from selenium.webdriver.support.wait import WebDriverWait
from retry import retry


class UserProfilePage(BasePage):
    def change_pwd(self, old_password, new_password, confirm_password):
        self.type(old_password, *UserProfileLocator.OLD_PASS)
        self.type(new_password, *UserProfileLocator.NEW_PASS)
        self.type(confirm_password, *UserProfileLocator.NEW_PASS_CONFIRM)
        self.click(*UserProfileLocator.SUBMIT)
        return HomeUserPage(self.driver)

    def is_success_popup_present(self):
        _d = self.driver
        try:
            WebDriverWait(_d, 5).until(lambda _d: _d.find_element(*UserProfileLocator.SUCCESS_POPUP))
        except Exception:
            return False
        return True

    def is_err_msg_pass_not_match(self):
        if "ng-active" in self.driver.find_element(*UserProfileLocator.ERR_MSG_PRESENT).get_attribute("class"):
            if self.driver.find_element(*UserProfileLocator.ERR_MSG_PASS_NOT_MATCH):
                return True
        return False

    def is_err_msg_pass_is_necessary(self):
        if "ng-active" in self.driver.find_element(*UserProfileLocator.ERR_MSG_PRESENT).get_attribute("class"):
            if self.driver.find_element(*UserProfileLocator.ERR_MSG_PASS_IS_NECESSARY):
                return True
        return False

    def get_expected_url(self):
        return UserProfileLocator.URL

    def get_problems_user_profile_page(self):
        if self.is_element_present(*ProblemsUserProfileLocator.PROBLEMS_TAB):
            self.click(*ProblemsUserProfileLocator.PROBLEMS_TAB)
            return UserProfileProblemsPage(self.driver)
        else:
            return None

    def get_comments_user_profile_page(self):
        if self.is_element_present(*CommentsUserProfileLocator.MY_COMMENTS_TAB):
            self.click(*CommentsUserProfileLocator.MY_COMMENTS_TAB)
            return CommentsUserProfilePage(self.driver)
        else:
            return None

    def get_admin_tab(self):
        self.click(*UserProfileNavigationLocator.ADMIN_TAB)

    def is_admin_tab_present(self):
        return self.is_element_present(*UserProfileNavigationLocator.ADMIN_TAB)

    def wait_until_page_is_loaded(self):
        self.wait_until_element_to_be_clickable(UserProfileNavigationLocator.PROBLEMS_TAB)

    @retry(tries=8, delay=2)
    def get_problems_page(self):
        self.wait_until_visibility_of_element_located(UserProfileProblemsLocator.PROBLEMS_TAB)
        self.click(*UserProfileNavigationLocator.PROBLEMS_TAB)
        return UserProfileProblemsPage(self.driver)

    def is_problems_tab_present(self):
        return self.is_element_present(UserProfileNavigationLocator.PROBLEMS_TAB)
