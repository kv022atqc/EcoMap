#!/usr/bin/env python
# -*- coding: utf-8 -*-

from framework.Locators import DetailedProblemLocator, HomePageLocator
from framework.pages.BasePage import BasePage
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class DetailedProblemPage(BasePage):

    def get_expected_url(self):
        return DetailedProblemLocator.URL

    def goToDetailedProblemPage(self):
        self.driver.get(DetailedProblemLocator.URL)
        self.wait_for_angular()

    def get_div_width(self):
        self.goToDetailedProblemPage()
        self.wait_for_angular()
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.presence_of_element_located((By.XPATH, DetailedProblemLocator.CLOSE_EL)))
        _width = self.driver.execute_script('''return document.querySelector('.fa.close').clientWidth''')
        return _width

    def get_div_height(self):
        self.goToDetailedProblemPage()
        self.wait_for_angular()
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.presence_of_element_located((By.XPATH, DetailedProblemLocator.CLOSE_EL)))
        _height = self.driver.execute_script('''return document.querySelector('.fa.close').clientHeight''')
        return _height

    def isCloseDivFixed(self):
        self.goToDetailedProblemPage()
        self.wait_for_angular()
        return  self.get_div_height() == self.get_div_width()

    def isPageClosed(self):
        self.driver.find_element_by_xpath(DetailedProblemLocator.CLOSE_EL).click()
        self.wait_for_angular()
        return self.get_current_url() == HomePageLocator.URL



