from locust import HttpLocust, TaskSet, task
from framework.Dictionary import DICTIONARY as test_data

class StatisticTest(TaskSet):

    @task(2)
    def log_out(self):
        with self.client.get("/api/logout", catch_response=True) as response:
            if response.status_code == 405:
                response.success()

    @task(5)
    def statistics_check(self):
        with self.client.get("/api/countSubscriptions", catch_response=True) as response:
            if response.status_code ==200:
                response.success()

        with self.client.get("/api/statistic_all", catch_response=True) as response:
            if response.status_code ==200:
                response.success()

    @task(3)
    def problems_check(self):
        with self.client.get("/api/problems_comments_statsall_permissions", catch_response=True) as response:
            if response.status_code >=403:
                response.success()

        with self.client.get("/api/problems_comments_statsresources?offset=0&per_page=10", catch_response=True) as response:
            if response.status_code ==404:
                response.success()

        with self.client.get("/api/problems", catch_response=True) as response:
            if response.status_code == 200:
                response.success()

class UserBehaviour(TaskSet):

    tasks = [StatisticTest]
    def on_start(self):
        self.sign_in_as_admin()

    def sign_in_as_admin(self):
        data = {"email": test_data.get('email'), "password": test_data.get('password')}
        self.client.get("/#/map")
        self.client.post("/api/login", json=data)


class WebUser(HttpLocust):
    task_set = UserBehaviour
    min_wait = 3 * 1000
    max_wait = 30 * 1000