from framework.Utils import *
from locust import HttpLocust, TaskSet, task
import json

class UserBehavior(TaskSet):

    def on_start(self):
        self.client.get("/")

    @task
    def press_go_to_registration_page_and_register(self):
        password = generate_random_word()
        data = {
                    "email": generate_random_word() + "@" + generate_random_word_without_digits(4) + ".com",
                    "first_name": generate_random_word_without_digits(int(generate_random_number(min_value=10, max_value=19))),
                    "last_name": generate_random_word_without_digits(int(generate_random_number(min_value=10, max_value=19))),
                    "nickname": generate_random_word(int(generate_random_number(min_value=6, max_value=20))),
                    "pass_confirm" : password,
                    "password" : password
        }
        self.client.post("/api/register", json=data)

class WebUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5 * 1000
    max_wait = 60 * 1000