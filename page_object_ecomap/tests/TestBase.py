import unittest
from framework.DriverWrapper import Driver
from framework.pages.HomePage import HomePage
from framework.Locators import BASE_URL


class TestBase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        base_url = BASE_URL
        cls.driver = Driver.get_driver()
        cls.home_page = HomePage(cls.driver, base_url)
        cls.home_page.open()
        Driver.mobile_remove_popup(cls.driver)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def tearDown(self):
        filename = "public/{}_teardown.png".format(self.id())
        if self.driver is not None:
            Driver.screenshot(self.driver, filename)
