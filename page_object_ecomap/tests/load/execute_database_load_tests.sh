#!/bin/bash

get_problem_by_id="SELECT p.id, p.title, p.content, p.proposal, p.severity,\
p.status, p.latitude,p.longitude, p.problem_type_id, p.created_date, \
t.name, p.is_enabled FROM problem AS p INNER JOIN problem_type AS t ON \
p.problem_type_id=t.id WHERE p.id=1;"

get_role="SELECT r.name FROM user_role AS ur \
                   JOIN user AS u ON ur.user_id=u.id \
                   JOIN role AS r ON ur.role_id=r.id \
                   WHERE ur.user_id=1;"

get_users_role_for_username_iamuser="SELECT u.id, u.first_name, u.last_name, r.name AS role_name\
 FROM user AS u INNER JOIN user_role AS ur INNER JOIN role AS r ON  u.id=ur.user_id AND r.id=ur.role_id\
 WHERE u.first_name = 'iamuser';"

get_problems_by_filter="SELECT p.id, p.title, p.latitude, p.longitude, p.problem_type_id, p.status, p.created_date, p.is_enabled, p.severity, p.user_id, pt.name, u.nickname FROM problem AS p INNER JOIN problem_type AS pt ON p.problem_type_id=pt.id INNER JOIN user AS u ON p.user_id = u.id WHERE user_id=1 ORDER BY 'nickname' ASC  LIMIT 0,5;"

get_user_can_delete="SELECT DISTINCT user.first_name, user.last_name FROM user INNER JOIN user_role ON user.id=user_role.user_id JOIN role_permission ON user_role.role_id=role_permission.role_id INNER JOIN permission ON role_permission.permission_id=permission.id AND permission.action='DELETE'"

mysqlslap --user=root --password=megasecret --host=127.0.0.1 \
--concurrency=300 --iterations=20 --create-schema=ecomap \
--query="$get_problem_by_id $get_users_role_for_username_iamuser $get_problems_by_filter $get_role $get_user_can_delete" \
--delimiter=";"

