#!/usr/bin/env python
# -*- coding: utf-8 -*-

from framework.Locators import CommentsUserProfileLocator, CommonLocator, UserProfileNavigationLocator
from framework.pages.BasePage import BasePage
from framework.pages.UserProfileProblemsPage import UserProfileProblemsPage
from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from retry import retry


class CommentsUserProfilePage(BasePage):

    def click_on_delete_btn(self, text):
        if (self.get_comments_count() > 5):
            self.click(*CommentsUserProfileLocator.LAST_BTN_ENABLED)
        self.click(*CommentsUserProfileLocator.get_delete_link_locator_by_title(text))

    def get_comments_count(self):
        count = int(self.get_text(*CommentsUserProfileLocator.COMMENTS_COUNT))
        return count

    def get_expected_url(self):
        return CommentsUserProfileLocator.URL

    def is_success_popup_present(self):
        return self.is_popup_present(*CommonLocator.SUCCESS_POPUP)

    def get_admin_tab(self):
        self.click(*UserProfileNavigationLocator.ADMIN_TAB)

    def wait_until_page_is_loaded(self):
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.presence_of_all_elements_located)

    @retry(tries=8, delay=2)
    def get_problems_page(self):
        """go to user's problems page
        please use this implementation of click() function on tab
        because errors occur sometimes with standard click(). To
        resolve this bug we have to simulate a mouse move action
        """
        actions = ActionChains(self.driver)
        problems_tab = self.find_element(*UserProfileNavigationLocator.PROBLEMS_TAB)
        actions.move_to_element(problems_tab).click().perform()
        return UserProfileProblemsPage(self.driver)

    def is_problems_tab_present(self):
        return self.is_element_present(UserProfileNavigationLocator.PROBLEMS_TAB)
