import json
from json import JSONDecodeError
from locust import TaskSet, task, HttpLocust, ResponseError
from framework.Dictionary import DICTIONARY as test_data
from framework.Utils import *


class CommentsLoadTest(TaskSet):

    def get_problem_id(self):
        response = self.client.get("/api/problems")
        try:
            response_result = json.loads(response.text)
            return response_result[0]['problem_id']
        except JSONDecodeError:
            return None

    @task(10)
    def add_comments(self):
        problem_id = self.get_problem_id()
        if problem_id is not None:
            add_comment_data = {"content": generate_random_word(length_of_word=15),
                                "problem_id": "%s" % problem_id,
                                "parent_id": "0",
                                "anonim": False}
            self.client.post("/api/problem/add_comment", json=add_comment_data)
        else:
            self.add_comments()

    @task(10)
    def add_sub_comments(self):
        problem_id = self.get_problem_id()
        comment_id = self.get_all_users_comments()
        if problem_id is not None and comment_id is not None:
            add_comment_data = {"content": generate_random_word(length_of_word=20),
                                "problem_id": "%s" % problem_id,
                                "parent_id": "%s" % comment_id,
                                "anonim": False}
            self.client.post("/api/problem/add_comment", json=add_comment_data)
        else:
            self.add_sub_comments()

    @task(5)
    def get_all_users_comments(self):
        with self.client.get("/api/all_users_comments?offset=0&per_page=5", catch_response=True) as response:
            try:
                response_result = json.loads(response.text)
                return response_result[0][0]['id']
            except JSONDecodeError:
                if response.status_code != 200 and response.status_code != 0:
                    raise ResponseError("Can not get user's comments, HTTP Status is " +
                                        str(response.status_code) + " " + response.reason)
            except (IndexError, KeyError):
                self.add_comments()
                self.get_all_users_comments()

    @task(4)
    def get_sub_comments(self):
        comment_id = self.get_all_users_comments()
        if comment_id is not None:
            self.client.get("/api/problem_subcomments/%s" % comment_id)
        else:
            self.get_sub_comments()

    @task(2)
    def change_comments(self):
        comment_id = self.get_all_users_comments()
        if comment_id is not None:
            self.client.post("/api/change_comment", json={"id": comment_id, "content": generate_random_word()})
        else:
            self.change_comments()

    @task(2)
    def delete_comment(self):
        comment_id = self.get_all_users_comments()
        if comment_id is not None:
            self.client.delete("/api/delete_comment?comment_id=%s" % comment_id)
        else:
            self.delete_comment()


class UserBehaviour(TaskSet):

    tasks = [CommentsLoadTest]

    def on_start(self):
        self.sign_in_as_admin()

    def sign_in_as_admin(self):
        self.client.get("/#/map")
        response = self.client.post("/api/login", json={"email": test_data.get('email'),
                                             "password": test_data.get('password')})
        if response.status_code != 200:
            self.sign_in_as_admin()


class WebUser(HttpLocust):
    task_set = UserBehaviour
    min_wait = 5 * 1000
    max_wait = 20 * 1000



