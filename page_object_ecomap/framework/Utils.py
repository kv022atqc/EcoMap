import random
import string


def generate_random_number(type_of_data='int', min_value=1, max_value=1000):
    if type_of_data == 'int':
        return str(random.randint(min_value, max_value))
    elif type_of_data == 'float':
        return str(random.uniform(min_value, max_value))


def generate_one_digit_number(type_of_data='int', min_value=1, max_value=9):
    if type_of_data == 'int':
        return str(random.randint(min_value, max_value))
    elif type_of_data == 'float':
        return str(random.uniform(min_value, max_value))


def generate_random_word(length_of_word=10):
    return ''.join(random.choice(string.ascii_lowercase + string.digits)for _ in range(length_of_word))


def generate_random_word_without_digits(length_of_word=10):
    return ''.join(random.choice(string.ascii_lowercase)for _ in range(length_of_word))


def generate_post():
    title = generate_random_word()
    type = generate_random_number(min_value=1, max_value=7)
    lat = generate_random_number('float', max_value=90)
    lon = generate_random_number('float', max_value=90)
    content = generate_random_word(length_of_word=20)
    proposal = generate_random_word(length_of_word=20)
    return {'title': title,
            'type': type,
            'latitude': lat,
            'longitude': lon,
            'content': content,
            'proposal': proposal
            }
