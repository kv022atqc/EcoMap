#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from datetime import timedelta
from framework.Locators import EditProblemLocator, CommonLocator
from framework.pages.SpikePage import HomeUserPage


class EditProblemPage(HomeUserPage):
    def edit_comment(self, new_text):
        self.type(new_text, *EditProblemLocator.NEW_COMMENT_TEXTAREA)
        self.press_enter(*EditProblemLocator.NEW_COMMENT_TEXTAREA)

    def is_cancel_edit_link_visible(self):
        return self.is_element_visible(*EditProblemLocator.COMMENT_CANCEL_EDIT_LINK)

    def click_on_edit_comment_link(self):
        self.click(*EditProblemLocator.COMMENT_EDIT_LINK)

    def add_comment_anonymous(self, text):
        self.click_on_anonymous_checkbox()
        self.add_comment(text)

    def add_comment(self, text):
        self.type_comment(text)
        self.click_on_add_comment_btn()

    def click_on_comment_tab(self):
        self.click(*EditProblemLocator.COMMENT_TAB)

    def type_comment(self, text):
        self.type(text, *EditProblemLocator.COMMENT_TEXTAREA)

    def is_comment_textarea_visible(self):
        return self.is_element_visible(*EditProblemLocator.COMMENT_TEXTAREA)

    def is_anonymously_checkbox_visible(self):
        return self.is_element_visible(*EditProblemLocator.ANONYMOUSLY_CHECKBOX)

    def is_add_comment_btn_visible(self):
        return self.is_element_visible(*EditProblemLocator.ADD_COMMENT_BTN)

    def click_on_add_comment_btn(self):
        self.click(*EditProblemLocator.ADD_COMMENT_BTN)

    def get_comment_nickname(self):
        return self.get_text(*EditProblemLocator.COMMENT_NICKNAME)

    def get_comment_datetime(self):
        if self.is_element_visible(*EditProblemLocator.COMMENT_DATETIME):
            element = self.find_element(*EditProblemLocator.COMMENT_DATETIME)
            return datetime.strptime(element.text, '%d/%m/%Y %H:%M')
        else:
            return None

    def get_comment_edit_datetime(self):
        if self.is_element_visible(*EditProblemLocator.COMMENT_EDIT_DATE_BLOCK):
            element = self.find_element(*EditProblemLocator.COMMENT_EDIT_DATE_BLOCK)
            date_text = element.text.replace("Редаговано · ", "")
            return datetime.strptime(date_text, '%d/%m/%Y %H:%M')
        else:
            return None

    def get_current_datetime(self):
        return datetime.strptime(datetime.now().strftime('%d/%m/%Y %H:%M'), '%d/%m/%Y %H:%M')

    def get_comment_text(self):
        return self.get_text(*EditProblemLocator.COMMENT_TEXT)

    def get_timedelta(self):
        return timedelta(seconds=60)

    def is_comment_answer_link_visible(self):
        return self.is_element_visible(*EditProblemLocator.COMMENT_ANSWER_LINK)

    def is_success_popup_present(self):
        return self.is_popup_present(*CommonLocator.SUCCESS_POPUP)

    def is_comment_edit_link_visible(self):
        return self.is_element_visible(*EditProblemLocator.COMMENT_EDIT_LINK)

    def is_comment_edit_link_invisible(self):
        return self.is_element_invisible(*EditProblemLocator.COMMENT_EDIT_LINK_HIDDEN)

    def is_comment_link_visible(self):
        return self.is_element_visible(*EditProblemLocator.COMMENT_LINK)

    def click_on_anonymous_checkbox(self):
        self.click(*EditProblemLocator.ANONYMOUSLY_CHECKBOX)

    def click_on_answer_link(self):
        self.click(*EditProblemLocator.COMMENT_ANSWER_LINK)

    def type_answer(self, text):
        self.type(text, *EditProblemLocator.ANSWER_TEXTAREA)

    def click_on_add_answer_btn(self):
        self.click(*EditProblemLocator.ADD_ANSWER_BTN)

    def is_answer_textarea_visible(self):
        self.is_element_visible(*EditProblemLocator.ANSWER_TEXTAREA)

    def get_answer_text(self):
        return self.get_text(*EditProblemLocator.ANSWER_TEXT)

    def get_answer_nickname(self):
        return self.get_text(*EditProblemLocator.ANSWER_NICKNAME)

    def get_number_of_comments(self):
        return self.get_number_of_elements(*EditProblemLocator.COMMENT_BLOCKS)

    def get_number_of_answers(self):
        return self.get_number_of_elements(*EditProblemLocator.ANSWER_BLOCKS)

    def add_answer(self, text, anonymous=False):
        self.click_on_answer_link()
        self.type_answer(text)
        if anonymous==True:
            self.click(*EditProblemLocator.ANSWER_ANONYMOUS_CHECKBOX)
        self.click_on_add_answer_btn()